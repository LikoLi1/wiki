---
title: Bordereau de livraison
description: 
published: true
date: 2021-05-26T12:36:26.594Z
tags: 
editor: markdown
dateCreated: 2021-05-26T12:32:26.117Z
---

# Bordereau de colis
Un Bordereau de livraison est un document qui répertorie les articles d'une expédition.

Il est généralement joint à la marchandise livrée.

À partir d'un seul bon de livraison, plusieurs bordereaux de livraison peuvent être créés. Il est utile lorsque l'envoi est emballé dans différentes boîtes. Chaque boîte peut avoir un poids et un nombre d'articles qu'elle contient. Par exemple, si vous expédiez 20 chaises dans 4 boîtes, chaque boîte peut contenir 5 chaises avec des bordereaux d'emballage différents pour chaque boîte.

---

Pour accéder à la liste des bordereaux d'expédition, allez sur:

> Accueil > Stock > Outils > **Bordereau de livraison**

![liste_bordereau_de_livraison.png](/stocks/packing-slip/liste_bordereau_de_livraison.png)

## 1. Prérequis avant utilisation

Avant de créer et d'utiliser un bon de livraison, il est conseillé de créer d'abord les éléments suivants:

- **Bon de livraison**

## 2. Comment créer un Bordereau de livraison

En règle générale, vous devez créer un bon de livraison à partir d'un bon de livraison lorsqu'il est au stade de l'ébauche, cependant, si vous souhaitez créer un bon de livraison manuellement, procédez comme suit.

1. Accédez à la liste **Bon de livraison**, cliquez sur Nouveau.
2. Sélectionnez le bon de livraison.
3. Entrez le numéro de colis de ce bon de livraison.
4. Cliquez sur le bouton **Obtenir les articles** pour récupérer les articles et les quantités dans le tableau Articles.
5. **Enregistrer**.

![créer_bordereau.png](/stocks/packing-slip/créer_bordereau.png)

La plupart de ces détails seront récupérés si vous créez le bon de livraison à partir du bon de livraison.

### 1.1 Options supplémentaires lors de la création d'un bon de livraison 

**Nombre de colis à expédier** : Si plusieurs colis du même type doivent être expédiés en même temps, définissez les numéros de colis De et À. Par exemple, colis numéros 1 à 5 dans un bordereau d'expédition, puis colis numéros 6 à 10 dans le prochain bordereau d'expédition et ainsi de suite. Cela s'affichera si vous imprimez ensuite le bon de livraison. Notez que cela ne fonctionnera que si votre envoi contient autant d'articles.

## 2. Caractéristiques 
### 2.1 Tableau des articles 

S'il s'agit d'un article par lots, vous devrez sélectionner le numéro de lot.
La quantité, l'unité de mesure, le poids net et l'unité de poids seront extraites du bon de livraison.
Saut de page crée un saut de page juste avant cet élément lors de l'impression.

![articles_bl.png](/stocks/packing-slip/articles_bl.png)

### 2.2 Détails du poids du colis 

Ces détails seront affichés lors de l'impression du bon de livraison.

**Poids net** : il est calculé comme la somme des poids de tous les éléments du tableau. 
**Poids brut** : Il s'agit du poids total final, y compris le poids des matériaux d'emballage utilisés. 
**Unité de mesure (Udm) poids brut** : Une UdM peut être définie ici pour le poids final du produit.

![détails_bl.png](/stocks/packing-slip/détails_bl.png)

### 2.3 Papier à en-tête

Vous pouvez imprimer votre bon de livraison sur le papier à en-tête de votre entreprise. En savoir plus ici 