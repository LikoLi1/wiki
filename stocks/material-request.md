---
title: Demande de matériel
description: 
published: true
date: 2021-05-05T13:39:25.822Z
tags: 
editor: markdown
dateCreated: 2021-05-05T09:47:58.127Z
---

# Demande de matériel

Une demande de matériel est un simple document identifiant une exigence d'un ensemble d'articles (produits ou services) pour une raison particulière.

Pour accéder à la demande de matériel, allez sur:

> Accueil > Stock > Demande de matériel

![demande_de_matériel_liste.png](/buying/demande_de_matériel_liste.png)

## Pré-requis nécessaires

Avant de créer et d'utiliser une demande de matériel, il est conseillé de créer d'abord les éléments suivants:

1. **[Un Fournisseur](/fr/buying/supplier)**
2. **[Un article](/fr/stocks/item)**

## Comment créer une demande de matériel

**L'objet de la demande**

Une demande de matériel peut avoir les objectifs suivants:

- **Achat** : Si le matériel demandé doit être acheté.
- **Transfert de matériel** : Si le matériel demandé doit être transféré d'un entrepôt à un autre.
- **Problème matériel** : Si le matériel demandé doit être émis à des fins telles que la fabrication.
- **Fabrication** : Si le matériel demandé doit être produit.
- **Fourni par le client** : Si le matériel demandé doit être fourni par le client. Pour en savoir plus à ce sujet, visitez la page **Article** fourni par le **client** .

![détails_pour_demande_de_matériel.png](/buying/détails_pour_demande_de_matériel.png)
