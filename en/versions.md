---
title: Versions
description: 
published: true
date: 2021-05-05T11:44:17.787Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:12:51.429Z
---

# Release notes

## Version 2

- [V2.1.0](/en/versions/V2_1_0)
- [v2.0.0](/en/versions/v2_0_0)
{.links-list}

## Version 1

- [v1.4.0](/en/versions/v1_4_0)
- [v1.3.0](/en/versions/v1_3_0)
- [v1.2.0](/en/versions/v1_2_0)
- [v1.1.0](/en/versions/v1_1_0)
- [v1.0.1](/en/versions/v1_0_0)
{.links-list}
