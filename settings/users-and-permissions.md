---
title: Utilisateurs et autorisations
description: 
published: true
date: 2021-06-12T08:01:46.953Z
tags: 
editor: markdown
dateCreated: 2021-05-10T14:50:47.385Z
---

# Utilisateurs et autorisations
Dans DOKOS, vous pouvez créer plusieurs utilisateurs et leur attribuer des rôles différents.

**Un rôle** est un ensemble d'autorisations attribuées à un utilisateur afin qu'il puisse accéder aux documents dont il a besoin. Par exemple, un employé des ventes aura besoin d'accéder aux transactions de vente, mais n'aura pas accès pour approuver les congés.

Certains utilisateurs ne peuvent accéder qu'à la partie publique de DOKOS (c'est-à-dire une vue de portail). Ces utilisateurs sont appelés **utilisateurs du site Web**. Les **utilisateurs système** auront accès aux modules et pourront accéder aux documents selon les rôles définis.

DOKOS implémente le contrôle des autorisations au niveau de l'utilisateur et du rôle. Chaque utilisateur du système peut se voir attribuer plusieurs rôles et autorisations. Le rôle le plus important est **Manager du sytème**. Tout utilisateur ayant ce rôle peut ajouter d'autres utilisateurs et définir des rôles pour tous les utilisateurs.

---
## 1. Utilisateurs et rôles

- [1. Ajouter un utilisateur](/fr/settings/users-and-permissions/user)
- [2. Rôle et profil de rôle](/fr/settings/users-and-permissions/role)
{.links-list}

## 2. Autorisations 

- [1. Autorisations basées sur les rôles](/fr/settings/users-and-permissions/permission-manager)
- [2. Autorisations des utilisateurs](/fr/settings/users-and-permissions/user-permissions)
- [3. Autorisation de rôle pour la page et le rapport](/fr/settings/users-and-permissions/role-permission-for-page-and-report)
{.links-list}


## 3. Configuration

- [1. Partager un document](/fr/settings)
- [2. Utilisateur limité](/fr/settings/users-and-permissions/limited-user)
- [3. Administrateur](/fr/settings/users-and-permissions/administrator)
{.links-list}
