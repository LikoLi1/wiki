---
title: Actions de flux de travail
description: 
published: true
date: 2021-05-19T13:24:07.782Z
tags: 
editor: markdown
dateCreated: 2021-05-13T17:24:30.741Z
---

# Actions de Flux de travail
Actions de flux de travail est un emplacement unique pour gérer toutes les actions en attente que vous pouvez effectuer sur les flux de travail.

Les actions de flux de travail enverront des notifications par e-mail uniquement si la case "Envoyer une alerte par e-mail" est cochée dans le flux de travail que vous avez créé.

---
Pour accéder aux **actions de flux de travail**, allez sur :

> Accueil > Paramètres > **Actions de workflow**

## 1. Les actions du flux de travail

Si un utilisateur est éligible pour prendre des mesures sur certains flux de travail, des e-mails seront envoyés à l'utilisateur avec le document pertinent en pièce jointe. De là, l'utilisateur peut **Approuver** ou **Rejeter** le flux de travail.

Les utilisateurs verront également les entrées dans leur liste d'actions de flux de travail :

![actions_flux_de_travail.png](/setup/workflows/actions_flux_de_travail.png)

### A savoir

- Vous pouvez définir un modèle d'e-mail pour les actions de workflow sur chaque état. Le modèle peut consister en un message permettant aux utilisateurs de procéder aux prochaines actions de flux de travail.
- Les actions de flux de travail ne seront pas créées pour une transition vers des états facultatifs.

