---
title: Valeurs par défaut globales
description: 
published: true
date: 2021-05-10T12:14:20.365Z
tags: 
editor: markdown
dateCreated: 2021-05-10T09:53:32.838Z
---

# Valeurs par défaut global

Les valeurs par défaut pour les documents tels que la devise, l'année fiscale, etc., peuvent être définies à partir des valeurs par défaut globales.

Les valeurs définies ici auront un impact sur tous les utilisateurs et les valeurs par défaut des différents champs seront définies pour eux.

---

Pour accéder aux **valeurs par défaut globales**, allez sur :

> Accueil > Paramètres > **Paramètres par défaut globaux**

Ou effectuez une recherche dans la barre de recherche.

## 1. Valeurs par défaut global

Chaque fois qu'un nouveau document est créé, ces valeurs seront définies par défaut.

- Société par défaut
- Exercice en cours
- Pays
- Unité de distance par défaut
- Devise par défaut
- Masquer le symbole monétaire
- Désactiver le Total arrondi
- Désactiver En lettres

L'unité de distance par défaut est utilisée pour calculer la distance totale dans les trajets de livraison.

![valeurs_par_défaut_-_société.png](/setup/global-defaults/valeurs_par_défaut_-_société.png)
