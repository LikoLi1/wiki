---
title: Administrateur
description: 
published: true
date: 2021-05-12T08:29:44.312Z
tags: 
editor: markdown
dateCreated: 2021-05-12T08:29:09.603Z
---

# Administrateur
**L'administrateur est au-dessus du Gestionnaire système et dispose de tous les droits et autorisations pour un compte DOKOS.**

Un gestionnaire système dispose également d'autorisations sur la plupart des éléments du système, mais l'administrateur dispose d'un accès illimité.

1. Si votre compte DOKOS est hébergé dans le cloud chez nous, vous ne pourrez pas accéder à votre compte DOKOS en tant qu'administrateur.

2. Pour les comptes hébergés dans le cloud, les mises à niveau sont gérées à partir du backend. Nous réservons les identifiants de connexion administrateur avec nous afin que nous puissions mettre à niveau tous les comptes DOKOS du client à partir du backend.

3. Étant donné que sur un seul serveur nous pouvons héberger de nombreux comptes DOKOS de clients, par mesure de sécurité, nous ne pouvons pas partager les informations d'identification du compte administrateur avec un utilisateur hébergé dans le cloud. (une exception serait si vous achetez un grand nombre d'utilisateurs et que votre compte est exclusivement hébergé sur un serveur).

4. Pour les comptes locaux auto-hébergés, les informations d'identification de l'administrateur sont avec l'utilisateur du compte.

