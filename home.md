---
title: Accueil
description: Outil de gestion pour les entreprises | ERP Français Open-Source
published: true
date: 2021-05-29T09:35:30.496Z
tags: 
editor: markdown
dateCreated: 2020-11-26T12:11:53.800Z
---

# Bienvenue sur DOKOS

Dokos est une adaptation d'un logiciel de gestion open-source bien connu: ERPNext.  
Bénéficiant de plus de 10 ans de développements et d'une utilisation dans des milliers d'entreprises, c'est un logiciel de gestion robuste et performant.

Dokos a été créé pour permettre d'adapter ce logiciel aux normes françaises et européennes et d'accélérer son développement en Europe.

Il est distribué sous licence GPLv3.

---

La documentation de dokos est en cours de création et vous pouvez participer à son amélioration en vous créant un compte sur ce site.


## Liens rapides

### Général

- [1. Introduction à l'ERP](/fr/introduction)
- [2. Démarrer avec Dokos](/fr/getting-started)
- [3. Contribuer](/fr/contributing)
- [4. Versions](/fr/versions)
- [5. Utilisation de l'ERP Dokos](/fr/tools)
{.links-list}

### Les modules

- [Achats](/fr/buying)
- [Comptabilité](/fr/accounting)
- [CRM](/fr/crm/)
- [Intégrations](/fr/integrations)
- [Lieu](/fr/venue)
- [Ressources Humaines](/fr/human-resources)
- [Site web](/fr/website)
- [Stocks](/fr/stocks)
- [Ventes](/fr/selling)
{.links-list}

---
Une partie du contenu est une adaptation de la [documentation d'ERPNext](https://docs.erpnext.com/)  
Licence: [CC 4.0 BY-SA-NC](https://creativecommons.org/licenses/by-nc-sa/4.0/)