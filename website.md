---
title: Site web
description: 
published: true
date: 2021-06-14T06:31:45.403Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:42:46.494Z
---

- [Formulaire web](/fr/website/web-forms)
- [Paramètres du panier](/fr/website/shopping-cart-settings)
- [Paramètres du site web](/fr/website/website-settings)
- [Portail](/fr/website/portal)
{.links-list}

> Les liens **Page d'accueil** et **Section de page d'accueil** ne sont plus visibles depuis le module site web car ces modules ont été retirés et ont été remplacés par **Page Web**. Il faut donc dès à présent passer par Site Web > **Page Web** pour créer une page d'accueil.
La page d'accueil est à configuré depuis les paramètres du site en indiquant le lien de la Page d'accueil. (Documentation plus détaillée à venir)
{.is-warning}


Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
