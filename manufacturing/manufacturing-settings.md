---
title: Paramètres de production
description: 
published: true
date: 2021-05-28T14:07:20.934Z
tags: 
editor: markdown
dateCreated: 2021-05-28T13:55:42.888Z
---

# Paramètres de production

Pour accéder aux **paramètres de production**, allez sur : 

> Accueil > Production > Paramètres > **Paramètres de production**

## 1. Autoriser la consommation de matériaux multiples

Si coché, plusieurs articles peuvent être utilisés pour un seul ordre de travail. Ceci est utile si un ou plusieurs produits chronophages sont en cours de fabrication. Par exemple, un seul produit prend un mois à fabriquer et les matières premières sont consommées quotidiennement. Dans un scénario régulier, cela ne sera pas possible avec des entrées de stock. L'activation de cette option vous permettra de créer des entrées de stock pour la consommation d'articles sans avoir à créer une entrée pour la postconsommation. Le résultat final est que vous pouvez voir le stock consommé dans les entrepôts et mettre à jour l'entrée de fabrication finale à un stade ultérieur.

![planification_de_capacité.png](/manufacturing/planification_de_capacité.png)

## 2. Planification de la capacité 

La planification de la capacité est le processus dans lequel une organisation décide d'accepter ou non les nouvelles commandes en fonction des ressources et des bons de travail existants.

![planification_de_capacité.png](/manufacturing/planification_de_capacité.png)

## 3. Entrepôt par défaut pour la production

Cet entrepôt sera mis à jour automatiquement dans le champ Entrepôt **Travaux en cours** des bons de travail.

![entrepôts_par_défaut_pour_la_production.png](/manufacturing/entrepôts_par_défaut_pour_la_production.png)

## 4. Entrepôt de produits finis par défaut

Cet entrepôt sera mis à jour automatiquement dans le champ **Entrepôt cible** de l'ordre de travail.

## 5. Sur-production pour les ventes et les ordres de travail

Lors de la création d'ordres de travail par rapport à une commande client, le système autorisera uniquement la quantité d'articles de production à être inférieure ou égale à la quantité de la commande client. Si vous souhaitez autoriser l'augmentation des ordres de travail avec une plus grande quantité, vous pouvez mentionner ici le pourcentage d'indemnité de surproduction.

**Exemple** : dans certains cas, un poste de travail doit fabriquer 100 unités pour la rentabilité, mais l'ordre de travail peut être de 50 unités. Dans ce cas, le pourcentage d'allocation de surproduction serait de 100.

![sur_production.png](/manufacturing/sur_production.png)

## 6. Mettre à jour automatiquement le coût de la nomenclature

Si coché, le coût de la nomenclature sera automatiquement mis à jour en fonction du taux de valorisation / tarif tarifaire / dernier taux d'achat des matières premières.

![autres_paramètres.png](/manufacturing/autres_paramètres.png)