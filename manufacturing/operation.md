---
title: Opération
description: 
published: true
date: 2021-05-31T10:15:57.713Z
tags: 
editor: markdown
dateCreated: 2021-05-31T09:39:24.168Z
---

# Opération

Une opération fait référence à toute opération de fabrication effectuée sur les matières premières pour les traiter plus loin dans le chemin de fabrication.

Le maître d'opération stocke une seule opération de fabrication, sa description et le poste de travail par défaut pour l'opération.

---

Pour accéder à la **liste des opérations**, accédez à:

> Accueil > Fabrication > Liste de matériaux > **Opération**

![liste_opération.png](/manufacturing/operation/liste_opération.png)

## 1. Prérequis avant utilisation

Avant de créer et d'utiliser une opération, il est conseillé de créer d'abord les éléments suivants:

- **Poste de travail**

## 2. Comment créer une opération

1. Allez dans la liste des opérations, cliquez sur **:heavy_plus_sign: Ajouter opération**.
2. Entrez un nom pour l'opération, par exemple, couper.
3. Sélectionnez le poste de travail par défaut sur lequel l'opération sera effectuée. Cela sera récupéré dans les nomenclatures et les bons de travail.
4. Si vous le souhaitez, ajoutez une description pour décrire ce que l'opération implique.
5. Enregistrer.

![créer_opération.png](/manufacturing/operation/créer_opération.png)

Une fois enregistrés, les éléments suivants peuvent être créés pour une opération :

- **Nomenclature**
- **Ordre de travail**
- **Carte de travail**
- **Feuille de temps**

![documents_liés_opération.png](/manufacturing/operation/documents_liés_opération.png)