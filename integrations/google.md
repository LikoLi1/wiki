---
title: Google Integration
description: 
published: true
date: 2020-11-27T07:14:57.094Z
tags: 
editor: undefined
dateCreated: 2020-11-26T16:29:43.307Z
---

# Google integration

__dokos__ can be integrated with several tools from the Google application suite: Google Calendar, Google Drive, Google Contacts and Google Maps

# Setup Google Settings

> Integrations > Google Settings

In order to integrate any of the Google tools with __dokos__, you need to activate and setup an OAUTH integration with the Google Cloud Platform.
This will then make it possible to obtain an access token (authorization to access the service) from Google for the service you wish to use.

### Setup for Calendar, Drive and Contacts

1. Go to [https://console.cloud.google.com/](https://console.cloud.google.com/)
1. Use an existing project or create a new project
1. In **API and Services** select **Credentials**
1. Click on **Create credentials** and select **OAuth Client ID**
![oauth_client_creation.png](/integrations/google/oauth_client_creation.png)
1. Select **Web Application**
1. In **Authorized Javascript origins** add your website URL: `https://{yoursite}` (e.g. __https://dokos.io__)
1. In **Authorized redirect URIs** add the following URLs depending on your integration needs:
    - Google Calendar: `https://{yoursite}?cmd=frappe.integrations.doctype.google_calendar.google_calendar.google_callback`
    - Google Contacts: `https://{yoursite}?cmd=frappe.integrations.doctype.google_contacts.google_contacts.google_callback`
    - Google Drive: `https://{yoursite}?cmd=frappe.integrations.doctype.google_drive.google_drive.google_callback`
1. Save and copy/paste the generated **Client ID** and **Client Secret** into the corresponding fields in the **Google Settings** document in __dokos__
1. Go to **OAuth consent screen** and add your domain to the **Authorized domains**
![oauth_consent_setup.png](/integrations/google/oauth_consent_setup.png)
1. Go to **Library** and enable the relevant APIS: **Contacts API**, **Google Calendar API** and/or **Google Drive API**

### Setup for Maps

Google Maps access doesn't require an OAuth connection, only a specific API key.

1. Go to [https://console.cloud.google.com/](https://console.cloud.google.com/)
1. Use an existing project or create a new project
1. In **API and Services** select **Credentials**
1. Click on **Create credentials** and select **API key**
1. Go to **Library** and activate the **Geocoding API**
1. Copy and paste this key into the **API key** field of the **Google Settings** document in __dokos__

# Google Calendar

> Integrations > Google Calendar

You can create as many Google Calendar as you wish.
Each calendar will be linked to only one reference document and optionally to a user.

Currently, the Google Calendar integration works with the following document types:
- Event
- Item Booking

### Create a new calendar

1. Give your calendar a meaningful name
1. Select a reference document: This calendar will only be selectable in the reference document.
1. Add a user if the option is provided. If a user is set, the calendar will only be accessible to this user.
1. Check your synchronization options
1. You can then **Authorize access to Google Calendar**
1. Once the access granted, you can add the Google Calendar ID of one of your existing calendars or leave this field empty.
  If empty, a new calendar will be automatically created.