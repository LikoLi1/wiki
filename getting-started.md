---
title: Démarrer avec Dokos
description: 
published: true
date: 2021-05-13T07:59:36.114Z
tags: 
editor: markdown
dateCreated: 2020-11-26T14:15:30.702Z
---

# Installation

- [Installation Rapide](/fr/getting-started/easy-installation)
- [Installation Docker *:construction:*](/fr/getting-started/docker-installation)
- [Migration ERPNext](/fr/getting-started/erpnext-migration)
- [Domain Setup](/fr/getting-started/domain-setup)
- [SSL Certificate](/fr/getting-started/ssl-certificate)
{.links-list}

# Mise en place

- [Configuration de la société](/fr/settings/company-setup)
- [Configurer les taxes](/fr/settings/setting-up-taxes)
- [Valeurs par défaut global](/fr/settings/global-defaults)
{.links-list}

# Maintenance

- [Mettre à jour Dokos](/fr/getting-started/updating-dokos)
{.links-list}

# Configuration

- [Formats d'impression](/fr/settings/print/print-formats)
- [Paramètrage des emails](/fr/getting-started/email-setup)
- [Modèles d'adresses](/fr/getting-started/address-templates)
{.links-list}