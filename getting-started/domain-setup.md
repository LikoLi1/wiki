---
title: Domain name
description: 
published: true
date: 2020-11-27T07:14:33.508Z
tags: 
editor: undefined
dateCreated: 2020-11-26T14:48:25.942Z
---

# DNS Multitenant

Once you have installed _Dokos_ you may want to activate its DNS Multitenant functionality and secure your website with [Let's encrypt](https://letsencrypt.org).

## Port based multitenancy

By default you new site is assigned to a specific port on your server.
The first one will be assigned to port 80, the second one to 81, etc.

To change the port assigned to a specific site, you can run:
`bench set-nginx-port {your site} {port number}`

For example:
`bench set-nginx-port examplesite.com 81`


## DNS based multitenancy

In order to simplify the management of your web server, you can activate the DNS Multitenant functionality.
This functionality allows you to simply name the folder containing your site like your domain name and let _Dodock_ handle the rest:

To activate the DNS Multitenant mode, launch:  
`bench config dns_multitenant on`

If you have already created a site, you can rename it with:  
`mv sites/{old site name} sites/{new site name}`

## Create a new site

To create a new site, you can launch:  
`bench new-site {your site}`

## Reload the web server

Once you have changed the port, activated the DNS Mulitenant mode or created a new site, you need to refresh the configuration for Nginx (web server).

1. Regenerate the config file: `bench setup nginx`
2. Reload Nginx: `sudo systemctl reload nginx` or `sudo service nginx reload`
