---
title: Easy Installation
description: Install Dokos on a new server
published: true
date: 2020-11-27T07:14:35.969Z
tags: 
editor: undefined
dateCreated: 2020-11-26T14:19:50.041Z
---

# Installation

- This is an opinionated setup meant to be run on a blank server.
- Works on Ubuntu 18.04+, CentOS 7+, Debian 8+
- You may have to install Python 3.6+ by running `apt-get install python3-minimal`
- You may also have to install build-essential and python-setuptools by running `apt-get install build-essential python3-setuptools`
- This script will install the pre-requisites, install docli and setup a dokos site
- Passwords for the System Administrator and MariaDB (root) will be asked
- You can then login as **Administrator** with the Administrator password

Open your Terminal and enter:

### Creation of a user

If you are on a fresh server and logged in as **root**, at first create a dedicated user for dokos
& equip this user with **sudo** privileges

```
  adduser [dokos-user]
  usermod -aG sudo [dokos-user]
```

And connect yourself to this user's session

    su - [dokos-user]

### Download the install script

For Linux:

    wget https://gitlab.com/dokos/docli/raw/master/playbooks/install.py


### Run the install script

Running the script for production:

    sudo python3 install.py --production --user [dokos-user]


> The applications are named Frappe and ERPNext because dokos is an adaptation of these software.  
> The underlying architecture is similar to these two software.  
> You can get more info on their respective documentation sites: [Frappe](https://frappe.io/docs), [ERPNext](https://erpnext.com/docs)

### What will this script do?

- Install all the pre-requisites
- Install the command line `bench`
- Create a new bench (a folder that will contain your entire dokos setup)
- Create a new dokos site on the bench

### How do I start dokos

Your process will be automatically setup and managed by `nginx` and `supervisor`.
If that's not the case, you can run from the bench folder:

    sudo bench setup production [dokos-user]

You can then connect to your server's address to start utilizing dokos.

> If your server's address is 57.69.123.1, enter it in your browser to start using dokos.
{.is-success}


---

Help
====

For bench help, you can type

    bench --help

Updating
========

To manually update the bench, run `bench update` to update all the apps, run
patches, build JS and CSS files and restart supervisor.

You can also run the parts of the bench selectively.

`bench update --pull` will only pull changes in the apps

`bench update --patch` will only run database migrations in the apps

`bench update --build` will only build JS and CSS files for the bench

`bench update --bench` will only update the bench utility (this project)

`bench update --requirements` will only update dependencies (python packages) for the apps installed