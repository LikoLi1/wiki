---
title: SSL Certificate
description: 
published: true
date: 2020-11-27T07:14:44.239Z
tags: 
editor: undefined
dateCreated: 2020-11-26T14:47:08.475Z
---

# Let's encrypt

You can bring your own certificate to secure your _Dokos_ installation or generate a new certificate using Let's encrypt

## Prerequisites

1. Your bench should be in DNS Multitenant mode
2. Your web domain should redirect to your site's server
3. You need to be able to execute the commands with root permissions

## Generate a new certificate

`sudo -H bench setup lets-encrypt {your site}`

## Generate a new certificate for a custom domain

`sudo -H bench setup lets-encrypt {your site} --custom-domain {your custom domain}`

## Renew a certificate

Each Lets Encrypt certificate is valid for 3 months. You can renew them 30 days before they expire.
The generation of a new domain automatically creates a cron job that attempts to renew your certificate automatically.
If this renewal is not successful, you can still renew your certificate manually running:

`sudo bench renew-lets-encrypt`