---
title: Ressources Humaines
description: 
published: true
date: 2021-06-14T14:36:28.579Z
tags: 
editor: markdown
dateCreated: 2020-11-27T07:14:12.770Z
---

# Gestion des congés

```mermaid
journey
    title Gestion des congés
    section Configuration
      Liste de congés: 5: RH
      Types de congés: 5: RH
      Périodes de congés: 5: RH
      Politiques de congés: 5: RH
    section Allocation de congés
    	Allocation de congés: 5: RH
      Feuilles de présence: 5: Employé
      Calcul des congés: 5: Dokos
    section Demandes de congés
     Demandes de congés: 5: Employé
     Validation de congés: 5: RH
```

## Cas d'usage
- [Premiers pas avec les congés](/fr/human-resources/leave-setup)
{.links-list}

## 1. Employé

- [Employé](/fr/human-resources/employee)
- [Type d'emploi](/fr/human-resources/employment-type)
- [Branche](/fr/human-resources/branch)
- [Département](/fr/human-resources/department)
- [Désignation](/fr/human-resources/designation)
- [Échelon de l'employé](/fr/human-resources/employee-grade)
- [Groupe d'employé](/fr/human-resources/employee-group)
- [Assurance maladie des employés](/fr/human-resources/employee-health-insurance)
{.links-list}

## 2. Cycle de vie des employés

- [Carte de compétence des employés](/fr/human-resources/employee-skill-map)
- [Transfert des employés](/fr/human-resources/employee-transfer)
- [Promotion des employés](/fr/human-resources/employee-promotion)
- [Départ des employés](/fr/human-resources/employee-separation)
{.links-list}

## 3. Gestion des quarts

- [Type de quart](/fr/human-resources/shift-type)
- [Sélection de quart](/fr/human-resources/shift-request)
- [Affectation de quart](/fr/human-resources/shift-assignment)
{.links-list}

## 4. Congés

- [Liste de congés](/fr/human-resources/holiday-list)
- [Type de congés](/fr/human-resources/leave-type)
- [Période de congés](/fr/human-resources/leave-period)
- [Politique de congés](/fr/human-resources/leave-policy)
- [Attribution de politique de congés](/fr/human-resources/leave-policy-assignment)
- [Demande de congés](/fr/human-resources/leave-application)
- [Allocation de congés](/fr/human-resources/leave-allocation)
- [Congés accumulés encaissés](/fr/human-resources/leave-encashment)
- [Liste de blocage des congés](/fr/human-resources/leave-block-list)
- [Demande de congé compensatoire](/fr/human-resources/compensatory-leave-request)
{.links-list}


## 5. Présences

- [Outil de gestion de présences](/fr/human-resources/employee-attendance-tool)
- [Présences](/fr/human-resources/attendance)
- [Demande de validation de présence](/fr/human-resources/attendance-request)
- [Outil de chargement de présences](/fr/human-resources/upload-attendance)
{.links-list}

## 6. Notes de frais

- [Notes de frais](/fr/human-resources/expense-claim)
- [Demande de déplacement](/fr/human-resources/travel-request)
- [Avance versées aux employés](/fr/human-resources/employee-advance)
{.links-list}

## 7. Recrutement

- [Offre d'emploi](/fr/human-resources/job-opening)
- [Proposition de poste](/fr/human-resources/job-offer)
- [Demandeur d'emploi](/fr/human-resources/job-applicant)
- [Promesse d'embauche](/fr/human-resources/appointment-letter)
- [Modèle de promesse d'embauche](/fr/human-resources/appointment-letter-template)
{.links-list}

## 8. Formation

- [Programme de formation](/fr/human-resources/training-program)
- [Événement de formation](/fr/human-resources/training-event)
- [Résultat de formation](/fr/human-resources/training-result)
{.links-list}

## 9. Gestion de la flotte

- [Chauffeur](/fr/human-resources/driver)
{.links-list}





Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
