---
title: Contributing
description: 
published: true
date: 2020-12-04T16:24:09.884Z
tags: 
editor: markdown
dateCreated: 2020-11-27T07:13:20.884Z
---

# How to contribute

- Answer questions on the community [Forum](https://community.dokos.io)
- Report issues linked to the software on [Gitlab](https://gitlab.com/dokos/dokos/issues)
- Contribute to this [documentation website](/en/contributing/documentation)
- [Translate](/en/contributing/translations) the software in your language
{.links-list}