---
title: v1.3.0
description: 
published: true
date: 2020-11-27T07:15:27.410Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:26:21.801Z
---

# V1.3.0

:tada: Read the release [blog post](https://dokos.io/en/blog/new-functionalities-version-1-3) :tada:

## dokos

#### Features
- Asset flow refactor
- Appointment scheduling tool in CRM
- New customer creation API for Stripe
- Account payable report can be filtered based on payment terms
- Addition of master data in the DATEV zip file [Germany]
- HSN Code wise Item Tax [India]
- Product search on website now includes meta fields
- New report for stock and accounting value comparison
- New bank reconciliation dashboard
- New hook `auto_reconciliation_methods` for custom automatic reconciliation methods
- The **Bank Reconciliation** doctype has been renamed to **Bank Clearance**.
- ESR auto reconciliation [Switzerland]
- Employee advance return
- Italian e-Invoicing
- Possibility to add custom fields in point of sales
- Possibility to create subscriptions plans with start and end date
- Activity summary in projects

#### Major Bug corrections
- Invoice submission performance issue improvement
- BOM UX improvement
- Valuation of "finished good" in purchase receipt fix
- Subject templates are now rendered in Email Campaign
- Pricing rules for quotations made to leads fix
- Improvements to Stripe and GoCardless subscription management

## dodock [Framework]

#### Features
- New Razorpay order API
- Upgrade of Font Awesome to v5
- Addition of Unicons
- New icons in list and form sidebars
- Complete removal of Selenium tests
- New option to create a section without bottom border in doctypes
- Standard print formats and notifications can now be disabled
- The desk top left icon is now customizable in system settings
- Possibility to use "before save" and "after save" methods in server scripts
- Possibility to allow the creation of a quick entry for a doctype from customize form
- Possibility to set custom Jinja methods from `jenv` hook
- Emails can now be sent via SSL
- Possibility to set a custom database schema in site_config
- Better messages when merging documents
- When cancelling a document, all linked documents are displayed in a popup and you can cancel them with a single click
- New hook `auto_cancel_exempted_doctypes` for single click cancel exceptions
- Possibility to login with [Fairlogin](https://www.fairkom.eu/en/fairlogin) using the `login_via_fairlogin` method
- Security headers in webhooks
- Multiple webhooks for a single DocType and event
- Possibility to not display seconds in datepicker
- Mandatory and readonly fields can be made dependent on other fields in the doctype form
- Google configuration can be added in site_config file
- Muuri grid updated to v0.8
- Changelog is not shown only to system managers

#### Major Bug corrections
- Emails are now RFC5322 compliants (Python 3)
