---
title: Liste des congés
description: 
published: true
date: 2021-06-07T07:29:18.373Z
tags: 
editor: markdown
dateCreated: 2021-06-04T12:17:21.981Z
---

# Liste des congés (Jours fériés)

Liste des congés est une liste qui contient les dates des vacances.

La plupart des organisations ont une liste de jours fériés standard pour leurs employés. Cependant, certains d'entre eux peuvent avoir des listes de jours fériés différentes en fonction de différents emplacements ou départements. Dans DOKOS, vous pouvez configurer plusieurs listes de congés et les attribuer à vos employés en fonction de vos besoins.

---

Pour accéder à **la liste des congés**, allez sur :

Accueil > Ressources humaines > Congés > **Liste des congés**

![liste_jours_de_congés.png](/humains-ressources/holiday-list/liste_jours_de_congés.png)

## 1. Comment créer une liste de vacances

1. Allez dans la liste des jours fériés, cliquez sur **:heavy_plus_sign: Ajouter Liste de congés**.
2. Saisissez le nom de la liste des jours fériés. Il peut être basé sur l'année fiscale ou l'emplacement ou le département selon l'exigence.
3. Sélectionnez **Date de début** et **Date de fin** pour la liste des jours férié.

![détails_congés.png](/humains-ressources/holiday-list/détails_congés.png)

## 2. Caractéristiques

Certaines des fonctionnalités supplémentaires de la liste des jours de congés sont les suivantes :

### 2.1 Ajout des vacances hebdomadaires

Vous pouvez rapidement ajouter des congés hebdomadaires dans la liste des jours fériés comme suit :

1. Dans la section **Ajouter des vacances hebdomadaires**, sélectionnez le jour de congé hebdomadaire.
2. Cliquez sur le bouton **Ajouter aux vacances**.
3. **Enregistrer**.

![ajouter_des_vacances.png](/humains-ressources/holiday-list/ajouter_des_vacances.png)


Une fois les congés hebdomadaires ajoutés, cela est reflété dans le tableau des jours fériés.

![jours_fériés.png](/humains-ressources/holiday-list/jours_fériés.png)

> **Remarque** : Vous pouvez ajouter plusieurs jours dans les congés hebdomadaires.
{.is-warning}

Vous pouvez également ajouter des jours spécifiques (comme les jours fériés) manuellement en cliquant sur l'option **Ajouter une ligne** dans le tableau Jours fériés.

> **Remarque** : chaque fois qu'un nouveau jour férié est mis à jour dans la table Jours fériés, le champ Nombre total de jours fériés est mis à jour.
{.is-warning}


### 2.2 Liste des jours fériés en entreprise

Vous pouvez définir une liste de jours fériés par défaut au niveau de l'entreprise dans la fiche de l'entreprise dans le champ **Liste des congés par défaut**.

![liste_de_congés_par_défaut.png](/humains-ressources/holiday-list/liste_de_congés_par_défaut.png)

### 2.3 Liste des jours fériés dans l'employé

Si vous avez créé plusieurs listes de jours fériés, sélectionnez une liste de jours fériés spécifique pour un employé dans le maître respectif.

Lorsqu'un employé demande un congé, les jours mentionnés dans la liste des jours fériés ne seront pas comptés, car ce sont déjà des jours fériés.

![employé_congé.png](/humains-ressources/holiday-list/employé_congé.png)

> **Remarque** : Si vous avez spécifié une liste de jours fériés dans la liste des employés, cette liste de jours fériés sera prioritaire par rapport à la liste de jours fériés par défaut de l'entreprise. Vous pouvez former autant de listes de vacances que vous le souhaitez. Par exemple, si vous avez une usine, vous pouvez avoir une liste pour les ouvriers de l'usine et une autre pour le personnel de bureau. Vous pouvez gérer entre plusieurs listes en liant une liste de congés à l'employé respectif.
{.is-warning}


### 2.4 Liste des jours fériés dans le poste de travail

Vous pouvez également définir une liste de jours de congés au niveau du poste de travail, comme indiqué dans la capture d'écran ci-dessous :

![atelier_congés.png](/humains-ressources/holiday-list/atelier_congés.png)

Les dates de la liste des jours fériés marquées dans le maître du poste de travail seront considérées comme les jours pendant lesquels le poste de travail restera fermé.



