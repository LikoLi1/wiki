---
title: Programme de formation
description: 
published: true
date: 2021-06-14T13:09:05.327Z
tags: 
editor: markdown
dateCreated: 2021-06-14T13:08:29.489Z
---

# Programme de formation

Le programme de formation définit des programmes conçus pour former des employés ou d'autres personnes à des compétences spécifiques.

Dans DOKOS, vous pouvez créer un programme de formation et planifier des événements de formation sous celui-ci.

---

Pour accéder à la liste de programme de formation, allez sur :

> Accueil > Ressources humaines > Formation > **Programme de formation**

![liste_programme_de_formation.png](/humains-ressources/training-program/liste_programme_de_formation.png)

## 1. Comment créer un programme de formation

1. Accédez à la liste des programmes de formation, cliquez sur **:heavy_plus_sign: Ajouter Programme de formation**.
2. Saisissez le **nom du programme de formation**.
3. Entrez le **nom du formateur**, l'**e-mail du formateur** et le **numéro de contact**.
4. Sélectionnez le fournisseur (facultatif) au cas où un fournisseur/expert externe aurait été appelé pour mener la formation.

![détails_programme_formation.png](/humains-ressources/training-program/détails_programme_formation.png)

De plus, vous pouvez également rédiger une brève description du programme de formation dans la case Description (facultatif).

> **Remarque** : Par défaut, le statut du programme de formation est « Planifié ». Cependant, vous pouvez modifier le statut en « Terminé » ou « Annulé » selon les besoins.
{.is-warning}

Une fois le programme d'entraînement enregistré, vous pouvez créer des événements d'entraînement sous celui-ci.