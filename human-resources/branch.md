---
title: Branche
description: 
published: true
date: 2021-06-02T09:48:09.080Z
tags: 
editor: markdown
dateCreated: 2021-06-02T09:35:32.379Z
---

# Branche

Une succursale est un point de vente d'une entreprise situé à un endroit différent, autre que le bureau principal.

DOKOS vous permet de créer et de conserver une trace des différentes branches de votre organisation.

Pour accéder à la liste des **Branches**, allez sur :

> Accueil > Ressources humaines > Employé > **Branche**

![branche_.png](/humains-ressources/branch/branche_.png)

## 1. Prérequis avant utilisation

Avant de créer une Branche, il est obligatoire de créer les documents suivants :

- **Compagnie**

## 2. Comment créer une succursale

1. Allez dans la liste Branche, cliquez sur **:heavy_plus_sign: Ajouter Branche**.
2. Entrez le **nom** de la succursale.
3. **Enregistrer**.

![créer_branche.png](/humains-ressources/branch/créer_branche.png)

Vous pouvez lier la succursale au maître des employés.


