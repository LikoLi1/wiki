---
title: Promotion des employés
description: 
published: true
date: 2021-06-03T13:41:29.740Z
tags: 
editor: markdown
dateCreated: 2021-06-03T13:41:29.740Z
---

# Promotion des employés

La promotion ou l'avancement de carrière est un processus par lequel un employé d'une entreprise se voit attribuer une part plus élevée de tâches, une échelle salariale plus élevée ou les deux.

Dans DOKOS, vous pouvez gérer la promotion des employés et ses différentes activités associées à l'aide de ce document.

Pour accéder à la liste de **promotion des employés**, allez sur :

> Ressources humaines > Cycle de vie des employés > **Promotion des employés**

![liste_promotion_employé.png](/humains-ressources/employee-promotion/liste_promotion_employé.png)

## 1. Prérequis avant utilisation

Avant de créer une promotion d'employé, il est conseillé de créer les documents suivants :

- **[Employé](/fr/human-resources/employee)**
- **[Département](/fr/human-resources/department)**

## 2. Comment créer une promotion d'employé

1. Accédez à la liste de Promotion des employés, cliquez sur **:heavy_plus_sign: Ajouter Promotion des employés**.
2. Sélectionnez l'**employé**.
3. Saisissez la **date de la promotion**.

![détails_promotion.png](/humains-ressources/employee-promotion/détails_promotion.png)

4. Dans le tableau **Détails de la promotion des employés**, sélectionnez la propriété et définissez la valeur actuelle et nouvelle.

![autres_info_promotion.png](/humains-ressources/employee-promotion/autres_info_promotion.png)
