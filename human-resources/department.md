---
title: Département
description: 
published: true
date: 2021-06-02T10:03:54.102Z
tags: 
editor: markdown
dateCreated: 2021-06-02T10:03:31.071Z
---

# Département

Un département est un domaine fonctionnel spécialisé ou une division au sein d'une organisation.

Vous pouvez configurer les départements de votre organisation, définir la liste de blocage des congés, ainsi que les approbateurs des congés et des dépenses pour les mêmes.

Pour accéder à la **liste Département**, allez sur :

> Accueil > Ressources humaines > Employé > **Département**

Le département est un maître arborescent, ce qui signifie que vous pouvez créer des départements et des sous-départements parents comme indiqué ci-dessous :

**Remarque** : La case à cocher **Est un groupe** doit être cochée si le service est un service parent.

## 1. Prérequis avant utilisation

Avant de créer un Département, il est conseillé de créer les documents suivants :

- **Compagnie**
- **Quitter la liste de blocage**

![département.png](/humains-ressources/department/département.png)

## 2. Comment créer un département ?

1. Allez dans la **liste des départements**, cliquez sur **:heavy_plus_sign: Ajouter Département**.
2. Entrez le **nom** du département.
3. Sélectionnez **Nom** de l'entreprise.
4. Sélectinnez le centre de coût pour la paie.
5. Sélectionnez Liste de blocage des congés (facultatif) applicable à ce département.
6. **Enregistrer**.

![nouveau_département.png](/humains-ressources/department/nouveau_département.png)

## 3. Caractéristiques

### 3.1 Approbateurs

Les approbateurs peuvent être définis depuis cette section pour attribuer un ou plusieurs approbateurs pour valider les demandes de congés, des notes de frais, des demandes de quart.

![approbateurs.png](/humains-ressources/department/approbateurs.png)







