---
title: Période de congés
description: 
published: true
date: 2021-06-08T07:35:26.579Z
tags: 
editor: markdown
dateCreated: 2020-11-27T07:16:15.978Z
---

# Période de congés

Une période de congés permet de définir les dates de début et de fin pour le calcul des congés d'un type de congés donné.  

> Les congés payés sont calculés du 01/06 au 31/05 et les RTT du 01/01 au 31/12.  
> Créez donc deux périodes de congés différentes pour ces deux types de congés.  
{.is-info}

---

Pour accéder à la liste de période de congé, allez sur :

> Accueil > Ressources humaines > Congés > **Période de congé**

![liste_période_de_congé.png](/humains-ressources/leave-period/liste_période_de_congé.png)

## 1. Prérequis avant utilisation

Avant de créer une Période de Congé, il est conseillé de créer les éléments suivants :

- **Compagnie**
- **Liste des jours fériés**

## 2. Comment créer une période de congé 

1. Allez dans la liste Période de congé, cliquez sur Nouveau.
2. Saisissez la date de début et la date de fin de la période de congé.
3. Sélectionnez le nom de l'entreprise pour laquelle la période de congé s'applique.
4. Enregistrer.

![créer_péridoe_de_congé.png](/humains-ressources/leave-period/créer_péridoe_de_congé.png)

La période de congé vous permet également de sélectionner une liste de jours fériés pour les congés facultatifs (facultatif) qui sera prise en compte pour l'attribution des congés facultatifs pour la période.

> **Remarque** : La « Liste des jours fériés pour les congés facultatifs » n'est pas la même que la « Liste des jours fériés » habituelle. Cette liste contiendra uniquement une liste de jours fériés facultatifs. La « Liste des jours fériés pour les congés facultatifs » peut être créée à partir du document Liste des jours fériés . Vous pouvez créer deux listes de jours fériés pour une période de congé ; l'un contenant les congés habituels et l'autre les congés facultatifs.
{.is-warning}

De plus, vous pouvez cocher la case « Est actif » si vous souhaitez activer cette période de congé particulière.

## 3. Accorder un congé en utilisant la période de congé

Dans la version 12, les congés pouvaient être accordés via la période de congé via le bouton « Accorder des congés ». Dans la version 13, les congés seront accordés à l'aide de Leave Policy Assignment .

Une fois les informations enregistrées, la Période de congé sera également utilisée comme un outil pour vous aider à accorder des congés à une catégorie d'employés.

Le bouton Accorder générera des allocations de congé en fonction de la politique de congé applicable à chaque employé. Vous pouvez attribuer des congés en fonction du grade de l'employé , du service ou de la désignation, comme indiqué ci-dessous.