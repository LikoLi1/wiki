---
title: Website settings
description: 
published: true
date: 2020-11-27T07:15:35.598Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:40:59.344Z
---

# Website settings

This configuration panel allows you to define the general settings for your website.  

## Make a custom signup page

> See also: [Web form](desk#List/Web Form/List)

By adding a link to a custom web form in the field "Custom signup form", you can replace the standard signup form with a custom web form.  

In order for your web form to behave similarly to the standard signup form, it needs to be linked with the User doctype and to contain at least the following fields:

- `email`: Email
- `first_name`: First Name

Make sure also that your form is published and that the option "Allow modifications" is checked.  
