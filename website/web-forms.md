---
title: Web forms
description: 
published: true
date: 2020-11-27T07:15:33.628Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:40:56.281Z
---

# Web Forms

You can authorize customers, suppliers or job applicants to access certain information or even create certain transactions on your __dokos__ instance.  
For example, you can let anyone create an account on your website (created with __dokos__ or connected via the API) and apply for a job. You can let your customers see the details of their invoices, issue tickets they have registered or give them the possibility to upload new documents directly themselves.

> There are two types of in-built interfaces available in __dokos__: The *Desk View* and the *Web View*.  
>  - The Desk is for users within your organization with access to potentially confidential data
>  - Web View is for users outside your organization who need to interact and to whom you want to provide a self-service access
> 
> Web forms are similar to the forms you generally fill in various websites on the internet. They are part of the *Web View* interface.
{.is-info}


## Create a new web form

To create a new **Web Form** go to:

> Website > Web Form

Select the **DocType** based on which you want to build your Web Form.  
You are simply giving a controlled access to this document type to your users and allowing them to modify part of this document or create a new one.  

The **Route** will be set based on the **Title** of your Web Form. You can also add an introduction text to show a friendly message above your form.

Add some fields to your Web Form.  
These are the fields from the DocType you have selected. You can change the labels of these fields.  
Try to keep the number of fields to a minimum as long forms are cumbersome to fill.  

Click on **See on Website** in the sidebar to view your Web form.

Checkboxes on the right:

1. **Published**: The web form will only be accessible if this is enabled.
2. **Login Required**: User can fill the Web Form only if they are logged in.  

    When Login Required is checked:  
    2.1. **Route to Success Link**: Go to the success link after the form is submitted.  
    2.2. **Allow Edit**: If this is unchecked the form becomes read-only once it is saved.  
    2.3. **Allow Multiple**: Allow user to create more than one record.  
    2.4. **Show as Grid**: Show records in a table format.  
    2.5. **Allow Delete**: Allow user to delete the records that he/she has created.  
    2.6. **Allow Comments**: Allow user to add comments on the created form.  

9. **Allow Print**: Allow user to print the document in the selected print format.
10. **Allow Incomplete Forms**: Allow user to submit form with partial data.

## Features
### Sidebar

You can also show contextual links in a sidebar on your Web Form.

### Creating Web Forms with child tables

You can add child tables to your web forms, just like in regular forms.


### Payment Gateway Integration

You can add a Payment Gateway to the web form.  
When your users save their form, they are redirected to a payment form.

### Portal User

In portal settings, you can set a role against each menu item so that only users with that role are allowed to see this item.


### Custom Script

You can write custom scripts for your Web Form for things like validating your inputs, auto-filling values, showing a success message, or any arbitrary action.

### Actions

You can add a text in the 'Success Message' field and this text will be shown to user once he successfully submits the web form.  
The user will be redirected to the URL set as 'Success URL' when clicked on 'Continue' button.  
This is only applicable to webforms accessible to unregistered users (webforms with 'Login Required' checkbox unchecked).


### Result

When a website user submits the form, the data will be stored in the document/doctype for which a web form is created.
