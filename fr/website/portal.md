---
title: Portail
description: 
published: true
date: 2020-12-02T14:13:27.023Z
tags: 
editor: markdown
dateCreated: 2020-12-02T14:13:27.023Z
---

Les portails sont des rubriques que l'ont peut mettre à disposition des utilisateurs de site web pour effectuer des actions en autonomie: Voir vos événements, accéder à leurs projets, récupérer leurs factures, etc...

# Réservations d'articles

Vous pouvez activer le portail "Bookings" pour donner un accès à vos clients à la liste des créneaux qu'ils ont réservés.  
Les différentes réservations apparaîtront avec les statuts "Confirmé", "Annulé" ou "Passé".

Si vous donnez l'autorisation d'écrire dans une réservation d'articles à vos clients, ceux-ci pourront également voir un bouton `Annuler` leur permettant d'annuler leur réservation.
Cette autorisation peut être donnée en cochant la case `Ecrire` pour le rôle `Client` (par défaut) dans le gestionnaire des rôles et autorisation.