---
title: Paramètres du site web
description: 
published: true
date: 2020-11-27T07:17:05.172Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:40:50.352Z
---

# Paramètres du site web

Ce panneau de configuration vous permet de définir les éléments généraux de paramétrage de votre site web.  

## Créer une page d'inscription personnalisée

En ajoutant un lien vers un formulaire web personnalisé dans le champ "Formulaire d'inscription personnalisé", vous remplacez le formulaire d'inscription standard par ce formulaire personnalisé.

Pour que cela fonctionne correctement, il faut que votre formulaire web soit lié au type de document Utilisateur et qu'il contienne au moins les champs suivants:

- `email`: Email
- `first_name`: Prénom

Assurez-vous également que votre formulaire est publié et que l'option "Autoriser les modifications" est cochée.
