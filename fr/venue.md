---
title: Lieu
description: 
published: true
date: 2020-12-02T16:41:44.441Z
tags: 
editor: markdown
dateCreated: 2020-11-26T16:48:16.179Z
---

# Gestion d'un lieu

Ce module a pour objet de regrouper les éléments principaux permettant de gérer un lieu physique. Celui-ci peut être constitué de bureaux, d'espaces de co-working, de salles de réunion, mais également de machines mises à disposition.

## Réservations d'articles

Les réservations d'article permettent d'enregistrer des réservations de créneaux horaires associés à un article. Cet article peut être une salle de réunion, un open-space ou une imprimante 3D.
L'article sert à la fois à facturer le client faisant une réservation et à enregistrer les créneaux associés à cette réservation.

- [Réservation d'articles](/fr/venue/item-booking)
{.links-list}


## Crédits de réservation

Les crédits de réservations permettent d'allouer des crédits à un client lorsqu'il achète un nombre d'heures ou de jours de réservations d'espace en avance afin de pouvoir les lui déduire lorsqu'il réserve effectivement un créneau.

- [Crédits de réservation](/fr/venue/booking-credits)
{.links-list}

## Abonnements

Les abonnements permettent de mettre en place une facturation récurrente pour un client.  
Vous pouvez par exemple créer un abonnement un forfait mensuel, ce qui veut dire que Dokos générera une facture par mois pour ce client jusqu'à annulation de l'abonnement.

- [Abonnements](/fr/selling/subscription)
{.links-list}

# Premiers pas

- [Configuration de Mon Tiers Lieu *Cas d'usage*](/fr/venue/use-case)
{.links-list}