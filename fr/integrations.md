---
title: Intégrations
description: 
published: true
date: 2021-01-27T14:29:04.634Z
tags: 
editor: markdown
dateCreated: 2020-11-26T16:38:48.410Z
---

# Paiements
- [GoCardless](/fr/integrations/gocardless)
- [Stripe](/fr/integrations/stripe)
- [Paypal](/fr/integrations/paypal)
{.links-list}

# Agendas / Drive / Emails / Contacts
- [Google](/fr/integrations/google)
{.links-list}

# Connecteurs
- [Zapier](/fr/integrations/zapier)
{.links-list}