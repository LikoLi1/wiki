---
title: Contribuer
description: 
published: true
date: 2020-12-04T16:22:14.723Z
tags: 
editor: markdown
dateCreated: 2020-11-27T07:14:05.997Z
---

# Comment contribuer

- Répondez à des questions sur le [Forum](https://community.dokos.io)
- Notifiez l'équipe de l'existence de bugs sur [Gitlab](https://gitlab.com/dokos/dokos/issues)
- Contribuez à ce site de [documentation](/fr/contributing/documentation)
- [Traduisez](/fr/contributing/translations) le logiciel dans votre langue
{.links-list}