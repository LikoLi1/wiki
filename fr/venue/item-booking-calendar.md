---
title: Calendrier de réservation d'articles
description: 
published: true
date: 2020-12-02T13:40:46.211Z
tags: 
editor: markdown
dateCreated: 2020-12-02T13:07:29.080Z
---

# Fonctionnement

Vous pouvez configurer autant de calendrier de réservation que vous le souhaitez par articles et unités de mesures.
La règle de sélection du calendrier correspondant à une réservation d'article est la suivante:

1. __Dokos__ cherche un calendrier correspondant à l'article et l'unité de mesure demandés
2. __Dokos__ cherche un calendrier correspondant à l'article demandé, sans unité de mesure
3. __Dokos__ cherche un calendrier correspondant à l'unité de mesure demandée, sans article
4. __Dokos__ cherche un calendrier lié à aucun article et aucune unité de mesure

Il est donc utile de configurer au moins un calendrier qui ne soit lié ni à un article ni à une unité de mesure, pour qu'il puisse servir de calendrier par défaut.

# Exemples

## Différents calendriers par unité de mesure

Vous louez vos espaces à l'heure, à la demi-journée et à la journée.  
Vous allez donc avoir a minima 2 calendriers:

> **Calendrier par défaut - A l'heure et à la journée**
> - **Unité de mesure**: Aucune - Ce calendrier sera valable pour toutes les unités de mesure, ce sera notre calendrier par défaut
> - **Article**: Aucun - Ce calendrier sera valable pour tous les articles
>
> |Jour|Heure de début|Heure de fin|
> |----|--------------|------------|
> |Lundi|08:00|18:00|
> |Mardi|08:00|18:00|
> |Mercredi|08:00|18:00|
> |Jeudi|08:00|18:00|
> |Vendredi|08:00|18:00|


> **Calendrier à la demi-journée**
> - **Unité de mesure**: Demi-journée - Ce calendrier ne sera valable que pour les demi-journées
> - **Article**: Aucun - Ce calendrier sera valable pour tous les articles
>
> |Jour|Heure de début|Heure de fin|
> |----|--------------|------------|
> |Lundi|08:00|13:00|
> |Lundi|13:30|18:30|
> |Mardi|08:00|13:00|
> |Mardi|13:30|18:30|
> |Mercredi|08:00|13:00|
> |Mercredi|13:30|18:30|
> |Jeudi|08:00|13:00|
> |Jeudi|13:30|18:30|
> |Vendredi|08:00|13:00|
> |Vendredi|13:30|18:30|
>
> *Si l'on considère que notre demi-journée dure 5 heures, la création de ce calendrier permet d'éviter les réservations de créneaux en dehors de ces horaires, comme par exemple 13:00-18:00*

## Différents calendriers par article

Vous louez une salle de réunion et un bureau, chacun ayant des horaires différents.

> **Calendrier de la salle de réunion**
> - **Unité de mesure**: Aucune - Ce calendrier sera valable pour toutes les unités de mesure
> - **Article**: Salle de réunion
>
> |Jour|Heure de début|Heure de fin|
> |----|--------------|------------|
> |Lundi|08:00|18:00|
> |Mardi|08:00|18:00|
> |Mercredi|08:00|18:00|
> |Jeudi|08:00|18:00|
> |Vendredi|08:00|18:00|


> **Calendrier du bureau**
> - **Unité de mesure**: Aucune - Ce calendrier sera valable pour toutes les unités de mesure
> - **Article**: Bureau
>
> |Jour|Heure de début|Heure de fin|
> |----|--------------|------------|
> |Lundi|07:00|19:00|
> |Mardi|07:00|19:00|
> |Mercredi|07:00|19:00|
> |Jeudi|07:00|19:00|
> |Vendredi|07:00|19:00|

Il est aussi possible de combiner des articles et des unités de mesure spécifiques.