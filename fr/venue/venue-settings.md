---
title: Paramètres du lieu
description: 
published: true
date: 2020-12-02T12:42:49.199Z
tags: 
editor: markdown
dateCreated: 2020-12-02T12:42:49.199Z
---

# Autoriser les réservations simultanées

Option permettant d'autoriser les réservations simultanées d'articles (plusieurs réservation d'un même article pendant le même créneau).
Cela active la possibilité d'indiquer un nombre de réservations simultanées autorisées pour chaque article dans les fiche d'articles.

# Supprimer les réservations dans le panier après x minutes

Paramétrage permettant de définir l'intervale de temps entre la dernière modification d'une réservation d'articles en statut "Dans le panier" et sa suppression automatique par le logiciel. Mettez 0 pour désactiver cette fonctionnalité.

# Synchroniser automatiquement avec Google Agenda

Dans les réservations d'article, la synchronisation des réservations avec Google Agenda est optionnelle.
En cochant cette case, le système synchronisera automatiquement toutes les réservations d'article avec Google Agenda.
Attention, cela ne fonctionne que si vous avez connecté votre système avec Google Agenda et créé un agenda pour chacun de vos articles réservables.

# Unité de mesure pour 1 minute et 1 mois

Si vous voulez autoriser des invités à sélectionner un créneau disponible sur votre site web, vous devez configurer l'unité de mesure correspondant à une minute et celle correspondant à un mois dans la section "Réservation d'articles" des paramètres du lieu.

Cela vous permet de créer vos propres unités de mesure et de les nommer comme vous le souhaitez.