---
title: Versions
description: 
published: true
date: 2020-11-27T07:14:25.576Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:11:24.808Z
---

# Notes de version

## Version 2

- [v2.0.0](/fr/versions/v2_0_0)
{.links-list}

## Version 1

- [v1.4.0](/fr/versions/v1_4_0)
- [v1.3.0](/fr/versions/v1_3_0)
- [v1.2.0](/fr/versions/v1_2_0)
- [v1.1.0](/fr/versions/v1_1_0)
- [v1.0.0](/fr/versions/v1_0_0)
{.links-list}
