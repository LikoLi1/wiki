---
title: Actifs
description: Immobilisations corporellles et incorporelles
published: true
date: 2020-12-21T08:04:17.529Z
tags: 
editor: markdown
dateCreated: 2020-12-21T07:47:33.124Z
---

# Démarrer

- [Enregistrer un nouvel actif immobilisé](/fr/assets/getting-started)
{.links-list}