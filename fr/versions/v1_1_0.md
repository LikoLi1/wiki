---
title: v1.1.0
description: 
published: true
date: 2020-11-27T07:16:53.073Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:26:00.022Z
---

# V1.1.0

:tada: Lire [l'article de blog](https://dokos.io/blog/nouveautes-version-1-1) :tada:

## dokos

#### Fonctionnalités
- Possibilité de poser des demi-journées de congés
- Nouveaux paramètres pour les exports DATEV
- Possibilité de permettre des réservations d'articles simultanées
- La période avant suppression des réservations d'articles en brouillon est désormais configurable
- La liste des réservations d'articles par utilisateur est désormais disponible sur le portail
- Modèle d'adresse spécifique pour la France (Nouvelles installations seulement. Pour les installations existantes, voir la documentation)
- Possibilité de mettre des valeurs dynamiques (en Jinja) dans les modèles de contrats

#### Corrections de bug
- Problèmes d'autorisations pour les abonnements générés depuis les webhooks
- Bug lors de la récupération des informations des prospects dans les opportunités et devis
- Corrections pour le connecteur Shopify
- Problèmes avec les adresses dans le panier
- Addition de créneaux avec deux unités de mesures différentes dans le panier
- Traduction de "variant of" dans le panier
- Nouveau message "Veuillez sélectionner une unité de mesure" lorsqu'aucune n'est sélectionnée

#### Modifications
- Le message par défaut pour les demandes de paiement situé dans les comptes de passerelle de paiement a été remplacé par un modèle d'email dans les modèles de passerelles de paiement.


## dodock [Modèle d'application]

#### Fonctionnalités
- [Scripts en python](/fr/dokos/installation/personnalisation/scripts-python)
- Centre de notification centralisé
- Vue en liste configurable
- Les emails non gérés de plus de 30 jours sont désormais supprimés toutes les nuits
- Possibilité d'ajouter des sauts de page dans les formats d'impression

#### Corrections de bug
- Correction du diagramme d'activité
- L'email de création d'un utilisateur depuis un contact était envoyé en double
- Problèmes de performance du bureau à cause d'une désactivation de cache
- Problèmes de formatage des devises
- Suppression automatique des caractères spéciaux lors de la création des liens de site web
- Possibilité de définir la largeur des colonnes en % dans les formats d'impression

#### Modifications
- Suppression du hook `jenv`


## Intégrations

:tada: Nouvelle application [Zapier](https://zapier.com) disponible en version beta.  
Envoyez-nous un email à [hello@dokos.io](mailto:hello@dokos.io) pour être invité.