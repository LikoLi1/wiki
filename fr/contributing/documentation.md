---
title: Compléter la documentation
description: 
published: true
date: 2020-12-04T16:21:19.117Z
tags: 
editor: markdown
dateCreated: 2020-12-04T16:21:19.117Z
---

# Contribuer à la documentation

Tous les utilisateurs de Dokos sont invités à partager leur expérience du logiciel en participant à la rédaction de cette documentation.

Les objectifs de ce site sont doubles:
1. Expliquer le fonctionnement du logiciel et décrire au maximum ses possibilités de paramétrage
2. Proposer le plus possible de cas d'usage

# Comment contribuer

Il suffit de se créer un compte sur ce site pour pouvoir commencer à éditer des pages.
Il existe actuellement deux niveaux d'édition:
1. **Editeur**: Vous pouvez éditer des pages existantes
2. **Créateur**: Vous pouvez créer de nouvelles pages

Si vous êtes un éditeur régulier, envoyez un email à hello@dokos.io pour obtenir le niveau **éditeur**.

# Quelles sont les règles à respecter

Ce site ayant vocation a être traduit en français et en anglais, si vous créez une nouvelle page en français, veillez à la créer dans l'autre langue également.
Mettez le texte suivant dans la page en anglais:

`:construction: Writing in progress :construction:` (:construction: Writing in progress :construction:)