---
title: Facture d'acompte
description: 
published: true
date: 2020-11-27T07:15:41.604Z
tags: 
editor: undefined
dateCreated: 2020-11-26T13:22:32.148Z
---

# Facture d'acompte

Une facture d'acompte permet de constater le versement d'une partie du prix d'une vente avant sa livraison ou d'un service avant sa réalisation.
Dans __Dokos__, il est possible d'enregistrer l'acompte et de le solder manuellement via une écriture de journal ou une écriture de paiement, mais il est également possible de laisser le logiciel faire cette écriture automatiquement en adoptant le flux suivant.


## Pré-requis

- Créez un article en cochant la case "Article d'acompte" et définissez un pourcentage d'acompte associé à cet article.  
Si vous avez plusieurs types d'acomptes avec des pourcentages différents, créez un article pour chaque type d'acompte.  

- Dans la fiche de la société, vérifiez que le champ "Compte d'acompte client par défaut" est bien renseigné.  
Le compte sélectionnné doit être de type "Débiteur".  
A ce jour, seuls les acomptes clients peuvent être automatisés. Les acomptes fournisseurs sont prévus dans une future version.  

> En France il s'agit d'une sous-catégorie du compte 419
{.is-info}


## Créez une facture depuis une commande client

Pour simplifier votre facturation, créez une commande client correspondant au montant final à facturer.  
Depuis cette commande client, cliquez sur "Créer > Facture".  

Dans la facture cochez la case "Facture d'acompte".  
En cochant cette case, vous déclenchez deux actions:

1. __Dokos__ récupère automatiquement l'article d'acompte, si vous n'en avez qu'un, et calcule le montant de l'acompte à partir du montant de la commande client.  
2. Le compte comptable débiteur du client est remplacé par le compte d'acompte client par défaut.  

> Si vous décochez la case "Facture d'acompte", le compte comptable débiteur est remplacé par celui du client.
{.is-info}


Lorsque vous validez la facture, aucune écriture comptable n'est créé. Seul le paiement sert à l'enregistrement comptable de l'acompte.  

## Créez le paiement correspondant à l'acompte

Créez une nouvelle écriture de paiement à partir de votre facture.  
Vérifiez que __Dokos__ a bien récupéré le compte d'acompte depuis la facture, ajoutez une référence et validez votre paiement.  

## Créez la facture finale

Pour créer votre facture finale, repartez de votre commande client et créez une nouvelle facture.  
Le montant à facturer est bien de la totalité de la facture.  

Allez dans la section "Acomptes et Avances" et cliquez sur le bouton "Récupérer les acomptes reçus".  
Votre acompte créé précédemment est automatiquement récupéré et le montant restant dû est mis à jour. Vous pouvez l'imprimer sur votre facture.  

## Créez le paiement final

Après validation de votre facture, créez un paiement.  
Le montant du paiement correspond au montant restant dû de votre facture, soit le montant total moins l'acompte.  

Validez votre paiement et vérifiez que __Dokos__ a bien contrepassé l'écriture de paiement correspondant à l'acompte et enregistré le montant total des deux paiements sur le compte client.  