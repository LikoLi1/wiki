---
title: Ventes
description: 
published: true
date: 2020-11-27T07:14:19.585Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:55:23.485Z
---

- [Clients](/fr/dokos/vente/client.md)
- [Liste de prix](/fr/dokos/vente/liste-de-prix.md)
- [Prix de l'article](/fr/dokos/vente/prix-article.md)
- [Devis](/fr/dokos/vente/devis.md)
- [Abonnements](/fr/dokos/vente/abonnement.md)
