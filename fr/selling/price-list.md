---
title: Liste de prix
description: 
published: true
date: 2020-11-27T07:16:36.959Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:55:36.031Z
---

# Liste de prix

Une liste de prix est un catalogue permettant de recenser tous les prix s'appliquant à une transaction donnée.  

Imaginons que vous vendez des meubles.  

Ces meubles vont peut-être avoir un prix de vente différent en fonction des types de clients auxquels vous les vendez.

Par exemple, vous allez peut-être vendre une table 100€ HT à un particulier qui passe dans votre boutique, alors que vous allez la vendre 60€ HT à un revendeur.

Dans ce cas vous aurez deux listes de prix et deux prix pour votre article **Table**:

- Liste de prix **Clients particuliers**
- Liste de prix **Revendeurs**

|Clients particuliers|Revendeurs|
|--------------------|----------|
|100€|60€|


## Liste de prix de vente

Pour définir une liste de prix de vente, créez une nouvelle liste de prix et cochez la case _Vente_

L'intérêt des listes de prix réside dans leur capacité à déterminer les prix automatiquement dans vos transactions.  
Par exemple, lorsque vous établissez un devis, vous voulez que le bon prix soit déterminé pour votre client en fonction de la liste de prix qui lui correspond.  

Une fois votre liste de prix créée, vous pouvez donc l'associer avec vos clients ou vos groupes de client.  

### Règles de détermination des prix de vente

Lors de la recherche d'une liste de prix correspond à votre client dans une transaction, le logiciel procède de la manière suivante:

1. Il cherche une liste de prix dans la fiche client
2. S'il n'en trouve pas, il cherche une liste de prix dans la fiche du groupe de clients associé à ce client
3. S'il n'a toujours pas trouvé de liste de prix, il prend celle définie dans les paramètres des ventes


## Liste de prix d'achat

Pour définir une liste de prix d'achats, créez une nouvelle liste de prix et cochez la case _Achats_

### Règles de détermination des prix d'achat

Le mode de fonctionnement est similaire aux liste de prix de vente.

1. Le logiciel cherche une liste de prix dans la fiche fournisseur
2. S'il n'en trouve pas il cherche une liste de prix dans la fiche du groupe de fournisseurs associé à ce fournisseur
3. S'il n'a toujours pas trouvé de liste de prix, il prend celle définie dans les paramètres des achats
