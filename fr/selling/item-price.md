---
title: Prix de l'article
description: 
published: true
date: 2020-11-27T07:16:32.890Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:55:29.963Z
---

# Prix de l'article

Les fiches "Prix de l'article" permettent de définir des prix, applicables pour une liste de prix donnée et valables pendant une période donnée.  

De plus, ces prix peuvent n'être applicables qu'en fonction de certains critères, comme par exemple, pour une unité de mesure donnée.  

> Nous vendons des chaises à l'unité et en lots de 4 à des particuliers et à des magasins.
>
> Nous aurons donc 4 prix:
>
> - 2 pour la liste de prix **Client particulier**
> - 2 pour la liste de prix **Revendeurs**

Et pour chacune de ces listes de prix, nous aurons un prix pour l'unité de mesure **Unité** et un prix pour l'unité de mesure **Lot de 4**


## Options

Un prix peut donc être déterminé pour un article et une liste de prix, sans spécificités additionnelles si celui-ci s'applique dans toutes les circonstances.  
Mais il est également possible de réduire le périmètre d'application de ce prix en définissant spécifiquement:

- Une unité de mesure
- Une unité d'emballage
- Un client ou un fournisseur

Si ces champs sont remplis, le prix ne s'appliquera que si la condition est remplie.

## Gestion des évolutions de prix

Vous pouvez définir des dates de validité pour vos prix.  
Celles-ci permettent de conserver la trace des différents prix applicables dans le temps, mais également de passer à une nouvelle tarification sans impact sur les factures encore à émettre avec l'ancien prix.
