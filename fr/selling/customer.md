---
title: Customer
description: 
published: true
date: 2020-11-27T07:16:30.971Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:55:26.815Z
---

# Client

Un client est une personne physique ou morale acheteur de vos produits ou services.

## Créer un client

> Vente > Client

Un client est une donnée de base, ne nécessitant aucun pré-requis. Cependant il est possible de créer un client à partir d'un [prospect](/fr/dokos/vente/prospect) existant dans _Dokos_.

### Paramétrages par défaut

Afin de faciliter la création de vos clients, il est possible de définir certaines informations par défaut dans les **paramètres de vente**.

1. **Système de nommage**: Chaque client possède un identifiant unique, qui est le nom du document dans lequel sont renseignés les informations du client. Cet identifiant du client peut être basé sur le nom complet du client ou sur un numéro de série.  
Si vous choisissez de le baser sur le nom complet du client, en cas d'homonymes, _Dokos_ rajoutera automatiquement un "-1" à la fin du nom pour les différencier.  
Si vous choisissez de le baser sur une série, vous pouvez définir le préfixe de cette série dans le type de document **Nom de série**. 

2. **Groupe de client**: Définissez le groupe de client par défaut. Il s'agit généralement du groupe de client le plus souvent sélectionné.

3. **Région par défaut**: Définissez la région qui sera sélectionnée par défaut dans le client.


### Fonctionnalités

Les informations renseignées dans la fiche client servent ensuite de valeurs par défaut dans les transactions.
Par exemple, si vous mettez la langue d'impression "fr" dans la fiche du client, les documents générés pour ce client (devis, factures, ...) seront imprimés par défaut en français. Vous pouvez toujours modifier cette valeur dans le document correspondant au moment de sa création.
