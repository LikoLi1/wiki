---
title: Abonnement
description: 
published: true
date: 2020-11-27T07:16:42.843Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:55:45.171Z
---

# Abonnement

Dokos permet la gestion de la facturation client récurrente et son intégration avec Stripe et GoCardless pour les paiements.

## Créer un abonnement

> Ventes > Abonnement

1. Sélectionnez un client
2. Sélectionnez une date de début d'abonnement ou une date de début de période d'essai
3. Sélectionnez les options correspondant à cet abonnement:
  - Génération d'une commande client au démarrage de la période
  - Génération de la facture au début de la période: par défaut la facture est générée à la fin de la période
  - Validation automatique de la facture après sa création: Si cette option n'est pas cochée, la facture reste en brouillon pour validation manuelle
4. Ajoutez vos plans d'abonnement
5. Ajoutez les règles de frais, taxes et livraisons correspondant à cette facture
6. Le cas échéant ajoutez les remises éventuelles à appliquer à la prochaine facture
7. N'oubliez pas les termes et conditions

### Plans d'abonnement

La notion de plan de d'abonnement consiste à lier un article avec une devise, une règle de détermination du prix et un intervale de facturation.
Vous pouvez créer autant de plans d'abonnement que nécessaire.

Dans une même facture vous ne pouvez ajouter que des plans d'abonnements ayant la même devise, la même règle de détermination du prix et le même intervale de facturation.

Si votre plan d'abonnement est lié à un plan de facturation Stripe, vous pouvez l'ajouter dans la section **plans de passerelles de paiement**.

### Dates de début et de fin

Chaque plan d'abonnement peut avoir une date de début et une date de fin d'abonnement.
Lors de la génération des factures, le plan doit être encore valide pour être pris en compte.
Il n'est pas possible de facturer des plans d'abonnement au pro-rata.

## Générer une demande de paiement pour le premier mois d'abonnement

> Tableau de bord > Demande de paiement 


## Passerelles de paiement

Lorsque le client paie son premier mois d'abonnement, la passerelle de paiement liée à cet abonnement est automatiquement mise à jour.
Elle permet la génération automatique des paiements suivants ou la liaison avec un webhook reçu depuis la passerelle de paiement.

## Annuler un abonnement

> Actions > Annuler l'abonnement

Mettez la date d'annulation de cet abonnement.
Si la dernière facture doit être générée au pro-rata des jours d'abonnements, cochez la case correspondante.
