---
title: Prospect
description: 
published: true
date: 2020-11-27T07:16:34.938Z
tags: 
editor: undefined
dateCreated: 2020-11-26T17:55:33.011Z
---

# Prospect

Un prospect est une personne physique ou morale présentant un intérêt pour les biens ou services que vous vendez.


# 1. Créer un prospect

> CRM > Prospect

Un prospect est une donnée de base ne nécessitant aucun pré-requis.


# 2. Fonctionnalités

## 2.1. Organisation ou personne physique

Si le prospect est une personne morale (entreprise), cochez la case "Le prospect est une organisation".
Dans ce cas, le nom de l'organisation doit obligatoirement être rempli. Vous pourrez ensuite lier autant de contacts que vous le souhaitez à ce prospect.

Si le prospect est une personne physique, il vous suffit de renseigner son nom dans la case correspondante.

Une fois votre prospect enregistré, dans la section "Adresse & Contact" vous pouvez ajouter une ou plusieurs adresses liées à ce prospect et un ou plusieurs contacts.
Si la case "Nom de la personne" est renseignée, un contact est automatiquement créé pour cette personne.
Vous pouvez aussi ajouter des informations concernant l'adresse et les coordonnées de votre prospect avant le premier enregistrement. Ces informations seront utilisées pour créer l'adresse et le contact liés à votre prospect.

Vous pouvez ainsi ajouter différents interlocuteurs à votre prospect au fur et à mesure de l'évolution de votre cycle de vente.

## 2.2. Suivi

En renseignant les informations relatives au prochain contact avec ce prospect, un événement est automatiquement ajouté à votre calendrier. Celui-ci vous enverra un email de rappel le matin du rendez-vous.
Vous pouvez également configurer des notifications en fonction de vos besoins spécifiques.


## 2.3 Informations additionnelles

Vous pouvez ajouter des informations complémentaires sur votre prospect dans les notes, mais également dans des champs d'information structurée qui seront facilement requêtables pour analyser vos prospects: part de marché, site web, industrie, région.
Ces informations seront reprises automatiquement à la création d'une fiche client à partir de ce prospect.

Le champ "Désabonné" est mis à jour automatiquement lorsque le prospect se désabonne des emails que vous lui envoyez. Il sert également à filtrer les prospects que vous ajoutez à un nouveau groupe email.
