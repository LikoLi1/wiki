---
title: Nom de domaine
description: 
published: true
date: 2020-11-27T07:16:03.171Z
tags: 
editor: undefined
dateCreated: 2020-11-26T14:45:20.365Z
---

# DNS Multitenant

Une fois que vous avez installé _Dokos_ vous voudrez certainement activer sa fonctionnalité DNS Multitenant and sécuriser votre site avec [Let's encrypt](https://letsencrypt.org/fr/).

## Multi-tenant basé sur les ports

Par défaut un port spécifique de votre serveur est attribué à votre nouveau site.
Le port 80 est attribué au premier, le port 81 au deuxième, etc...

Pour modifier le port attribué à un site, vous pouvez lancer:
`bench set-nginx-port {votre site} {numéro de port}`

Par exemple:
`bench set-nginx-port siteexemple.com 81`


## Multi-tenant basé sur l'adresse DNS

Afin de simplifier la gestion de votre serveur web, vous pouvez activer la fonctionnalité DNS Multitenant.
Cette fonctionnalité vous permet de simplement nommer le dossier contenant votre site comme votre nom de domaine et de laisser _Dodock_ gérer le reste:

Pour activer le mode DNS Multitenant, lancez:
`bench config dns_multitenant on`

Si vous avez déjà créer un site, vous pouvez le renommer avec:
`mv sites/{ancien nom du site} sites/{nouveau nom du site}`

## Créer un nouveau site

Pour créer un nouveau site, vous pouvez lancer:
`bench new-site {votre site}`

## Relancez le serveur web

Une fois que vous avez modifié le port, activé le mode DNS Multitenant ou créé un nouveau site, vous devez mettre à jour la configuration de Nginx (serveur web).

1. Regénérez le fichier de configuration: `bench setup nginx`
2. Rechargez Nginx: `sudo systemctl reload nginx` ou `sudo service nginx reload`