---
title: Prospect
description: 
published: true
date: 2021-05-18T10:38:18.629Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:55:33.011Z
---

# Prospect

Un prospect est un client potentiel qui pourrait être intéressé par vos produits ou services.

Pour amener le client à franchir la porte, vous pouvez effectuer tout ou partie des actions suivantes :

- **Liste de votre produit sur des répertoires.**
- **Maintenir un site Web mis à jour et consultable.**
- **Rencontrer des gens lors d'événements commerciaux.**
- **Publicité de vos produits ou services.**

Lorsque vous faites savoir que vous êtes dans les parages et que vous avez quelque chose de précieux à offrir, les gens viennent vérifier vos produits. Ce sont vos leads.

Ils sont appelés **Prospects** (Lead en anglais) car ils peuvent vous conduire à une vente. Les directeurs des ventes travaillent généralement sur les pistes en les appelant, en établissant une relation et en envoyant des informations sur les produits ou services. Il est important de suivre toute cette conversation pour permettre à une autre personne qui pourrait avoir à faire le suivi de ce contact. La nouvelle personne est alors en mesure de connaître l'historique de cette piste particulière.

---

Pour accéder à la liste des prospects, allez sur :
> CRM > **Prospect**

![liste_prospect.png](/crm/lead/liste_prospect.png)

# 1. Créer un prospect

1. Accédez à la liste des prospects et cliquez sur :heavy_plus_sign: Ajouter Prospect
	- **Le prospect est une organisation** : Cochez la case si vos prospect est une organisation. Notez que si la case est cochée, le champ **Nom de l'entreprise est obligatoire**.

2. **Remplir les différents champs pour complèter la fiche** :
	- Nom de la personne
	- Nom de l'organisation
	- Adresse email
	- Responsable du prospect
	- Statut du prospect : Ouvert, Répondu, Opportunité, Devis...
	- Salutation : Mme, M., etc.
	- Désignation
	- Genre
	- Source
	- Nom de la campagne
3. **Enregistrer**

![créer_un_prospect.png](/crm/lead/créer_un_prospect.png)

# 2. Caractéristiques

## 2.1 Suivre

En renseignant les informations relatives au prochain contact avec ce prospect, un événement est automatiquement ajouté à votre calendrier. Celui-ci vous enverra un email de rappel le matin du rendez-vous.
Vous pouvez également configurer des notifications en fonction de vos besoins spécifiques.

![suivre_un_prospect.png](/crm/lead/suivre_un_prospect.png)

## 2.2 Notes

Dans cette section, vous avez la possibilité d'ajouter toutes les informations nécessaires sur le prospect, des remarques, des suggestions afin de faciliter la gestion du prospect.


## 2.3 Adresse et contact

### 2.3.1 Adresse du prospect

Vous pouvez indiquer l'**adresse** de votre prospect :

- Type d'adresse : Facturation, livraison, Magasin, Bureau, Entrepôt, etc.
- Titre de l'adresse : Domicile, Travail, etc.
- Adresse, Ville, État, Pays, Code postal

![adresse_prospect.png](/crm/lead/adresse_prospect.png)

### 2.3.2 Contact du prospect

Indiquez les **coordonnées** du prospect : 

- Téléphone
- N°Mobile
- Fax
- Site web

![contact_prospect.png](/crm/lead/contact_prospect.png)

## 2.3 Informations additionnelles

Vous pouvez ajouter des informations complémentaires sur votre prospect dans les notes, mais également dans des champs d'information structurée qui seront facilement requêtables pour analyser vos prospects: part de marché, site web, industrie, région.
Ces informations seront reprises automatiquement à la création d'une fiche client à partir de ce prospect.

Le champ "Désabonné" est mis à jour automatiquement lorsque le prospect se désabonne des emails que vous lui envoyez. Il sert également à filtrer les prospects que vous ajoutez à un nouveau groupe email.
