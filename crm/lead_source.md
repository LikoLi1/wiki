---
title: Source du prospect
description: 
published: true
date: 2021-05-20T12:19:40.546Z
tags: 
editor: markdown
dateCreated: 2021-05-20T12:19:40.546Z
---

# Sources du prospect
Une source de leads est une source à partir de laquelle les leads sont générés.

Il est important de suivre la source à partir de laquelle vous obtenez les prospects. Cela aidera à mesurer l'efficacité des campagnes de marketing et à allouer les budgets en conséquence.

Pour accéder à la **liste des sources de prospects**, allez sur :

> Accueil > Vendre > Paramètres > **Source de prospect**

![liste_source_du_prospect.png](/crm/liste_source_du_prospect.png)

## 1. Comment créer une source de prospect

1.Allez dans la liste source du prospect, cliquez sur :heavy_plus_sign: .
2. Entrez **Nom de la source**.
3. Entrez **Détails**.

![créer_source_prospect.png](/crm/créer_source_prospect.png)

Les sources de prospects typiques sont les formulaires de contact sur votre site Web, les campagnes marketing, les événements, les références, les publicités sur les réseaux sociaux, etc.