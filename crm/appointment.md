---
title: Rendez-vous
description: 
published: true
date: 2021-05-19T08:00:39.102Z
tags: 
editor: markdown
dateCreated: 2021-05-19T08:00:39.102Z
---

# Rendez-vous
Un rendez-vous est une réunion préétablie entre un responsable et un employé de votre entreprise.

Le type de document de rendez-vous peut être utilisé pour planifier et gérer l'interaction avec un prospect ou une opportunité.

---

Pour accéder à la **liste des rendez-vous**, allez sur :

> Accueil > CRM > Pipeline des ventes > **Rendez-vous**

![liste_rdv.png](/crm/appointment/liste_rdv.png)

## 1. Prérequis avant utilisation

Avant de créer un Rendez-vous, il est conseillé de créer ces différents documents : 

- 1. **Paramètres de réservation de rendez-vous**
- 2. **Liste de vacances**
- 3. **Employé**
- 4. **Prospect**
- 5. **E-mail**

## 2. Comment créer un rendez-vous ?

### 2.1 Créeation rapide

1. Allez dans la liste des Rendez-vous, cliquez sur **:heavy_plus_sign: Ajouter Rendez-vous**.
2. Définir l'heure du rendez-vous dans le champ **Heure programmée**
3. Indiquez le **statut**.
4. Saisir le **nom** de la personne pour le rendez-vous.
5. **Email** du contact.

![création_rapide_rdv.png](/crm/appointment/création_rapide_rdv.png)

### 2.2 Création complète

1. Allez dans la liste des Rendez-vous, cliquez sur **:heavy_plus_sign: Ajouter Rendez-vous**.
2. Faire **modifier en pleine page**
3. Remplir les différents champs pour complèter la **fiche Rendez-vous**
- Heure programmée
- Statut du Rendez-vous
- Nom, Numéro de télphone, ID Skype, Email
- Détails sur le rendez-vous et sur la personne

![création_rdv.png](/crm/appointment/création_rdv.png)

## 3. Caractéristiques

### 3.1 Documents liés

- **Rendez-vous avec**
- **Tiers**
- **Événement du calendrier**

![détails_rdv.png](/crm/appointment/détails_rdv.png)



