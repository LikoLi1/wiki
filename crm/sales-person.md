---
title: Vendeur
description: 
published: true
date: 2021-05-20T07:24:53.929Z
tags: 
editor: markdown
dateCreated: 2021-05-20T07:05:10.895Z
---

# Vendeur
Le vendeur est une personne qui vend vos produits / services.

Les commerciaux sont créés de manière hiérarchique dans DOKOS. Vous pouvez créer des vendeurs et ajouter d'autres vendeurs sous les vendeurs principaux.

---

Pour accéder à la **liste des commerciaux**, allez sur :

> Accueil > Ventes > **Vendeur**

![liste_vendeur.png](/crm/sales-person/liste_vendeur.png)

Il est également accessible depuis: Accueil> CRM> Paramètres> Commercial

## 1. Comment créer un vendeur ?

1. Allez dans la liste des commerciaux et cliquez sur **:heavy_plus_sign: Nouveau**.
2. Saisissez le **nom** du vendeur.
3. Sélectionnez l'**employé** du vendeur.
4. Cochez la case **Nœud de groupe** si vous souhaitez ajouter plus de vendeur sous ce vendeur.
5. Cliquez sur **Créer nouveau**.

![créer_vendeur.png](/crm/sales-person/créer_vendeur.png)

6. Pour complèter la fiche du vendeur, allez sur la liste et sélectionner le nom du vendeur et faire **Modifier**.

![modifier_vendeur.png](/crm/sales-person/modifier_vendeur.png)

7. **Remplir les différents champs pour complèter la fiche vendeur** :
	- Commercial Parent
	- Taux de commission
	- Est un groupe : Cochez la case si le vendeur fait partie d'un groupe
	- Saisir les objectifs commerciaux
  
![nom_et_id_vendeur.png](/crm/sales-person/nom_et_id_vendeur.png)

## 2. Caractéristiques

### 2.1 Vendeur dans les transactions

Vous pouvez utiliser les opérations de vente du vendeur comme la commande client, le bon de livraison et la facture client.

### 2.2 Attribuer des cibles de vente au vendeur

Vous pouvez attribuer un objectif de vente au commercial et suivre la progression. Consultez Allocation cible des commerciaux pour plus de détails.

![objectifs_vendeur.png](/crm/sales-person/objectifs_vendeur.png)
