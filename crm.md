---
title: CRM
description: 
published: true
date: 2021-05-25T08:31:38.489Z
tags: 
editor: markdown
dateCreated: 2021-05-18T09:21:20.059Z
---

# CRM
DOKOS vous aide à suivre les opportunités commerciales des prospects et des clients, à leur envoyer des devis et à réserver des commandes client .

## 1. Fonctionnalités de base

- [1. Prospect](/fr/crm/lead)
- [2. Opportunité](/fr/crm/opportunity)
- [3. Client](/fr/crm/customer)
- [4. Vendeur](/fr/crm/sales-person)
- [5. Adresse](/fr/crm/address)
- [6. Contrat](/fr/crm/contact)
- [7. Rendez-vous interne](/fr/crm/appointment)
{.links-list}

## 2. Configuration

- [1. Différence entre Prospect, Client et Contact](/fr/crm/difference_between_lead_contact_and_customer)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.

