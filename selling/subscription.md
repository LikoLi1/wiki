---
title: Abonnement
description: 
published: true
date: 2021-05-18T08:37:03.048Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:55:45.171Z
---

# Abonnement

Dokos permet la gestion de la facturation client récurrente et son intégration avec Stripe et GoCardless pour les paiements.

---

Pour accéder à la liste des abonnements, allez sur : 
> Accuei > Ventes > **Abonnement**

![liste_abonnement.png](/sales/subscription/liste_abonnement.png)

## 1. Comment créer un abonnement

1. Cliquer sur en haut à droite sur le bouton **:heavy_plus_sign: Ajouter Abonnement** pour créer un nouveau
2. Sélectionnez un **client**
3. Indiquez la**société** et choisir la **devise** de transaction
4. **Enregistrer**

![création_abonnement.png](/sales/subscription/création_abonnement.png)

### Caractéristiques

### 2.1 Période d'abonnement
La période d'abonnement vous permet de définir la date de début de l'abonnement ou de définir une date pour commencer une période d'essai. 

- Sélectionnez une **date de début d'abonnement** ou une **date de début de période d'essai**

![plans_abonnement.png](/sales/subscription/plans_abonnement.png)

### 2.2 Paramètre d'abonnement

Dans la section **Paramètre**, vous allez choisir les différentes options possibles pour cet abonnement : 

   - **Génération d'une commande client** au démarrage de la période
   - **Génération de la facture** au début de la période: par défaut la facture est générée à la fin de la période
   - **Validation automatique de la facture après sa création** : Si cette option n'est pas cochée, la facture reste en brouillon pour validation manuelle
   - **Choisir l'intervalle de facturation** de l'abonnement : Jour, Semaine, Mois ou Année
   - **Définir la fréquence de facturation** : Nombre de fois que votre abonné va recevoir la facture de l'abonement. Par exemple, si vous avez choisi un intervalle par **mois** et une fréquence sur **1**, il recevra une facture par mois.
   - **Le nombre de jours avant échéance** : Nombre de jours maximum pendant lesquels l'abonné peut payer les factures générées par cet abonnement avant d'être considéré en retard.
   
![paramètre_d'abonnement.png](/sales/subscription/paramètre_d'abonnement.png)

### 2.3 Plans d'abonnement

La notion de plan de d'abonnement consiste à lier un article avec une devise, une règle de détermination du prix et un intervale de facturation.
Vous pouvez créer autant de plans d'abonnement que nécessaire.

Dans une même facture vous ne pouvez ajouter que des plans d'abonnements ayant la même devise, la même règle de détermination du prix et le même intervale de facturation.

Si votre plan d'abonnement est lié à un plan de facturation Stripe, vous pouvez l'ajouter dans la section **plans de passerelles de paiement**.

![plans_abonnement.png](/sales/subscription/plans_abonnement.png)

#### 2.3.1 Dates de début et de fin

Chaque plan d'abonnement peut avoir une date de début et une date de fin d'abonnement.
Lors de la génération des factures, le plan doit être encore valide pour être pris en compte.
Il n'est pas possible de facturer des plans d'abonnement au pro-rata.

## 2.4 Taxes

Pour ajouter des taxes à votre abonnement, vous devez sélectionner un **Modèle de frais et taxes de vente** ou ajouter des taxes manuellement dans le tableau des taxes et frais de vente.

Le total des taxes et frais est affiché sous le tableau. En cliquant sur __Répartition des taxes__ vous afficherez toutes les composantes de taxes et leurs montants.

Vous pouvez également ajouter une **Règle de livraison** pour les articles dans votre abonnement.

![taxes_abonnement.png](/sales/subscription/taxes_abonnement.png)

## 2.5 Réductions

En plus d'offrir des remises par article, vous pouvez ajouter une remise à la totalité de l'abonnement dans cette section.  
Cette remise peut être basée sur le total TTC ou sur le total HT.  
La remise additionnelle peut être définie en pourcentage ou en montant.

![réductions_abonnement.png](/sales/subscription/réductions_abonnement.png)

## 2.6 Termes et conditions

Vous pouvez sélectionner un modèle de termes et conditions et le modifier en fonction des spécificités de la transaction.


## 2.7 Générer une demande de paiement pour le premier mois d'abonnement

> Tableau de bord > Demande de paiement 

## 2.8 Passerelles de paiement

Lorsque le client paie son premier mois d'abonnement, la passerelle de paiement liée à cet abonnement est automatiquement mise à jour.
Elle permet la génération automatique des paiements suivants ou la liaison avec un webhook reçu depuis la passerelle de paiement.

## Annuler un abonnement

> Actions > **Annuler l'abonnement**

Mettez la date d'annulation de cet abonnement.
Si la dernière facture doit être générée au pro-rata des jours d'abonnements, cochez la case correspondante.
