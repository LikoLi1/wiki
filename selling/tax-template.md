---
title: Modèles de taxes
description: 
published: true
date: 2021-05-17T12:28:17.824Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:55:48.047Z
---

# Modèles de taxes d'article


## Le modèle de taxe d'articles est utile pour la taxation des articles.

Si certains de vos articles ont des taux de taxe différents du taux de taxe standard attribué dans le tableau Taxes et frais, vous pouvez créer un modèle de taxe d'article et l'affecter à un article ou à un groupe d'articles. Le taux attribué dans le modèle de taxe sur les articles aura la préférence sur le taux de taxe standard attribué dans le tableau Taxes and Charges.

Par exemple, si la taxe GST 18% est ajoutée dans le tableau Taxes and Charges de la commande client, elle sera appliquée à tous les articles de cette commande client. Cependant, si vous devez appliquer un taux de taxe différent sur certains articles, les étapes sont indiquées ci-dessous.

Pour accéder à la liste des modèles de taxe sur les articles, accédez à
> Accueil > Comtabilité > Taxes > Item Tax Template

Supposons que nous créons une commande client. Nous avons le modèle de modèle de taxes de vente et de frais pour la TPS 9%. Sur tous les articles en vente, sur un article, seulement 5% de TPS sera appliqué, tandis qu'un autre article est exonéré de taxe (non taxable). Vous devez sélectionner le responsable du compte de la taxe et définir son taux de dérogation.

## Comment créer une Modèle de taxes d'article 

- Accéder à la page de la liste de **Modèle de taxes d'article**
- Cliquer sur en haut à droite sur le bouton **+ Ajouter Modèle de taxes d'article**
- **Titre** et **Société**: Nommer cette modèle et indiquer sa société
- **Tax rate**: Selectionner la type de tax et idiquer le Tax Rate. Pour l'article qui est entièrement exonéré de taxe, indiquez 0% comme taux de taxe dans la fiche article.
![modele_de_taxe_d'article.png](/sales/modele_de_taxe_d'article.png)
