---
title: Région
description: 
published: true
date: 2021-05-17T12:03:34.425Z
tags: 
editor: markdown
dateCreated: 2021-05-17T12:03:15.713Z
---

# Région de vente
Une région de vente est une zone géographique dans laquelle vous faites des affaires.

Dans DOKOS, une région est utilisé pour classer les clients, les adresses, dans le rapport comptable et les objectifs de vente alloués.

---

Pour accéder à la **liste des Régions**, allez sur :

> Accueil > Vendre > Paramètres > **Région**

![liste_région_de_vente.png](/sales/territory/liste_région_de_vente.png)

## 1. Comment créer un territoire 
1. Allez dans la liste Territoire, cliquez sur **:heavy_plus_sign: Nouveau**.
2. Cochez **Noeud de groupe** s'il y aura des sous-territoires sous ce territoire. Par exemple, la France est un territoire de groupe et Grand-Est est un sous-territoire.
3. **Enregistrer**.

![création_région_de_vente.png](/sales/territory/création_région_de_vente.png)

Vous pouvez ajouter plusieurs sous-territoires sous un territoire parent. Lors de l'enregistrement, un territoire peut être sélectionné dans les transactions et les rapports.

## 2. Caractéristiques

### 2.1 Affecter un Responsable de territoire

Vous pouvez affecter un responsable de territoire qui s'occupe des ventes de cette région.

### 2.2 Définition des objectifs de vente

Ici, vous pouvez définir des objectifs de vente spécifiques basés sur les champs suivants :

- Groupe d'articles
- Exercice fiscal
- Qté cible
- Montant cible
- Distribution cible

![champs_régions_de_vente.png](/sales/territory/champs_régions_de_vente.png)