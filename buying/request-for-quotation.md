---
title: Appel d'offre
description: 
published: true
date: 2021-05-07T08:58:31.722Z
tags: appel d'offre
editor: markdown
dateCreated: 2021-05-05T09:55:09.170Z
---

# Appel d'offre
Un Appel d'offre (Demande de devis fournisseur) est un document qu'une organisation (entreprise, association, particulier...) envoie à un ou plusieurs fournisseurs pour demander un devis pour des articles.

```diagram
PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSI4NDJweCIgaGVpZ2h0PSIzMjJweCIgdmlld0JveD0iLTAuNSAtMC41IDg0MiAzMjIiIGNvbnRlbnQ9IiZsdDtteGZpbGUgaG9zdD0mcXVvdDtlbWJlZC5kaWFncmFtcy5uZXQmcXVvdDsgbW9kaWZpZWQ9JnF1b3Q7MjAyMS0wNS0wN1QwODoyNjozNS42MTFaJnF1b3Q7IGFnZW50PSZxdW90OzUuMCAoTWFjaW50b3NoOyBJbnRlbCBNYWMgT1MgWCAxMF8xNV83KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvOTAuMC40NDMwLjkzIFNhZmFyaS81MzcuMzYmcXVvdDsgdmVyc2lvbj0mcXVvdDsxNC42LjEwJnF1b3Q7IGV0YWc9JnF1b3Q7Y2t3NE1KTUZXb2JHUjRNa2RQU2ImcXVvdDsgdHlwZT0mcXVvdDtlbWJlZCZxdW90OyZndDsmbHQ7ZGlhZ3JhbSBpZD0mcXVvdDtsaWMxb1RMOTBwQ2tJNS1hX2xPciZxdW90OyBuYW1lPSZxdW90O1BhZ2UtMSZxdW90OyZndDs3VnJmYzVzNEVQNXJtTHMrM0EwQ1N1eEh4NDU3RDNjek41ZVpYdnVvd0JyVUFPS0VpTzM3NnlzaHlmeHVQTEdEM2FZd2syR1hsWkMrM2YyMGttTzV5M1QzZ2VFOC9vdUdrRmlPSGU0c2QyVTV6dHp6eFYrcDJDdUY0M296cFlrWUNaVU8xWXA3OGo5b3BhMjFKUW1oYUJseVNoTk84cll5b0ZrR0FXL3BNR04wMnpiYjBLVDkxUnhIMEZQY0J6anBhLzhsSVkrMUZ0bDIvZUlQSUZGc1BuMWozcVRZV0d0RkVlT1FiaHNxOTg1eWw0eFNycDdTM1JJU2laNEJSclZiajd3OWpJeEJ4bzlxb0pGL3drbXBaNmNIeHZkbXV0dVljTGpQY1NEbHJYQ3A1ZDdHUEUyRWhNVGpoaVRKa2lhVUNUbWptVEM2TFRpamoyQ1VsaU1NNVgxNFkxQVQwNzFsdE14Q0NIVm5VWUlMNlIxYlBHTVdhTi83UXRMakJNWmhOenBaZElCUUJCL1FGRGpiQ3hQZHdOT2diMnVuell3dWJ2akxOZDdCT2xDaVExYzFsT0pCb3ptTXJQTThzQkNLc05JaVpUeW1FYzF3Y2xkckcrQklRR3FiUHluTk5XSmZnUE85eGdtWG5IYWNRek91WHlJcEovZ0JFdEZSQ0t6aG5ubDFhZk0xVGtraVUvTWpzQkJuK0lDOUhPNjNrUmV6b3lVTHRKV25VeE96Q0xTVlArd2ZCZ25tNUtuZCt5bmd1MWNKL2dpNjQ5bHlOdHlSTXhId1hnLzRGYVJZd0NqNUhoUURXa3ZYV3N3WmthdEN4eWx0TW5pR2VOcGNJalU1TUNLR0RFdzJJbG1rWGRjRGVPbkpXNzR4OUZ1WlBRSVBZaTAwaVdqRWI1Vjc5ZGp0MFZ6RHdXTlVUYXZEa1dmZ3M1bW1xWDFiYk5BYjhnZm96VHNEdTczL25oTHNaV25rOTlOb05sRVcrUVBvK29rWXhPMkRlSWg0TlNlbGtGT3VTaDBUM1A1L0pWVUdyaTJ1emFhcFVtMFhlYTRxc2wvb1pzUEFkQ1ZHcFhwcmYwR29HMTk5dXduYnJIUEVrTzVtOG42VlJENWs3UlNaZkhPMW1kd0EyNjZ1NlpiUVV4TmROLzJia2lvN3Rac2R2K05udStNL3hUVzZWZTNDQldONDN6RExwVUZ4L0hlTVhFZUU2ckdPajhNY2p3cVovcDVoQlUra3FMWlNKY3RJVVVESjNqSlZuSjhTaHZZdXIwWUo4NnVraFBFY1ArdXlqNXlCOHRtZGFPRTNKeHhYQnYxRXJEc0kvYWxieGhjeHFEZkMxT3RqN2Izek1pNUNsd2dENFN1Mi82VGJWOEpuS2Z6KzNvaXJYZlBsYWo5Q2xwY01IMitxekIwNjcxRUZkSkhqck9VN1U0OUxYSDRyS3FBV3dnQ2hmTmN2MWp1Ri9ndDdXZExVYk1OYmEvUnpsYjhhKzFzby9zK3dabDl5UTQ0R2pyeVdyaldiQjR6d2toM09YM0pNSUlYSzEycER5WTRPcWlvd09uSDE2eUtnYWM3aDNZOGRISzllN2wxMEM0ajZwM2IvUUhWS2QxTld4d1E0aURILzZlSFRmbnpvZUhqS2doNWQ1M0hkVkJYOXdFa2V1cG1xTHVpZjVhMXhZQmg1MnN5NlhQNk1KM1huVkcxZFhhK1RjY2laTXVVR3p0Vyt2UjcvWk5jVGZPMVA2R3NoMWovUXE4MWMvWDhPN3QxWCZsdDsvZGlhZ3JhbSZndDsmbHQ7L214ZmlsZSZndDsiPjxkZWZzLz48Zz48cmVjdCB4PSIxIiB5PSIxIiB3aWR0aD0iODQwIiBoZWlnaHQ9IjMyMCIgcng9IjE5LjIiIHJ5PSIxOS4yIiBmaWxsPSJub25lIiBzdHJva2U9IiNkMWQxZDEiIHN0cm9rZS13aWR0aD0iMiIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxwYXRoIGQ9Ik0gMTIxIDEyMSBMIDEyMSAxNTQuNjMiIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzAwMDAwMCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSAxMjEgMTU5Ljg4IEwgMTE3LjUgMTUyLjg4IEwgMTIxIDE1NC42MyBMIDEyNC41IDE1Mi44OCBaIiBmaWxsPSIjMDAwMDAwIiBzdHJva2U9IiMwMDAwMDAiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxwYXRoIGQ9Ik0gMjAxIDEwMSBMIDIzNC42MyAxMDEiIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2QxZDFkMSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSAyMzkuODggMTAxIEwgMjMyLjg4IDEwNC41IEwgMjM0LjYzIDEwMSBMIDIzMi44OCA5Ny41IFoiIGZpbGw9IiNkMWQxZDEiIHN0cm9rZT0iI2QxZDFkMSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PHJlY3QgeD0iNDEiIHk9IjgxIiB3aWR0aD0iMTYwIiBoZWlnaHQ9IjQwIiByeD0iNiIgcnk9IjYiIGZpbGw9IiNmZmZmZmYiIHN0cm9rZT0iI2M0YzRjNCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiAxNThweDsgaGVpZ2h0OiAxcHg7IHBhZGRpbmctdG9wOiAxMDFweDsgbWFyZ2luLWxlZnQ6IDQycHg7Ij48ZGl2IHN0eWxlPSJib3gtc2l6aW5nOiBib3JkZXItYm94OyBmb250LXNpemU6IDA7IHRleHQtYWxpZ246IGNlbnRlcjsgIj48ZGl2IHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogMTFweDsgZm9udC1mYW1pbHk6IFZlcmRhbmE7IGNvbG9yOiAjMDAwMDAwOyBsaW5lLWhlaWdodDogMS4yOyBwb2ludGVyLWV2ZW50czogYWxsOyB3aGl0ZS1zcGFjZTogbm9ybWFsOyB3b3JkLXdyYXA6IG5vcm1hbDsgIj5EZW1hbmRlIGRlIG1hdMOpcmllbDwvZGl2PjwvZGl2PjwvZGl2PjwvZm9yZWlnbk9iamVjdD48dGV4dCB4PSIxMjEiIHk9IjEwNCIgZmlsbD0iIzAwMDAwMCIgZm9udC1mYW1pbHk9IlZlcmRhbmEiIGZvbnQtc2l6ZT0iMTFweCIgdGV4dC1hbmNob3I9Im1pZGRsZSI+RGVtYW5kZSBkZSBtYXTDqXJpZWw8L3RleHQ+PC9zd2l0Y2g+PC9nPjxwYXRoIGQ9Ik0gMTIxIDIwMSBMIDEyMSAyMzQuNjMiIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzAwMDAwMCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSAxMjEgMjM5Ljg4IEwgMTE3LjUgMjMyLjg4IEwgMTIxIDIzNC42MyBMIDEyNC41IDIzMi44OCBaIiBmaWxsPSIjMDAwMDAwIiBzdHJva2U9IiMwMDAwMDAiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxyZWN0IHg9IjQxIiB5PSIxNjEiIHdpZHRoPSIxNjAiIGhlaWdodD0iNDAiIHJ4PSI2IiByeT0iNiIgZmlsbD0iI2U4ZThlOCIgc3Ryb2tlPSIjYzRjNGM0IiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSAtMC41KSI+PHN3aXRjaD48Zm9yZWlnbk9iamVjdCBzdHlsZT0ib3ZlcmZsb3c6IHZpc2libGU7IHRleHQtYWxpZ246IGxlZnQ7IiBwb2ludGVyLWV2ZW50cz0ibm9uZSIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ij48ZGl2IHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sIiBzdHlsZT0iZGlzcGxheTogZmxleDsgYWxpZ24taXRlbXM6IHVuc2FmZSBjZW50ZXI7IGp1c3RpZnktY29udGVudDogdW5zYWZlIGNlbnRlcjsgd2lkdGg6IDE1OHB4OyBoZWlnaHQ6IDFweDsgcGFkZGluZy10b3A6IDE4MXB4OyBtYXJnaW4tbGVmdDogNDJweDsiPjxkaXYgc3R5bGU9ImJveC1zaXppbmc6IGJvcmRlci1ib3g7IGZvbnQtc2l6ZTogMDsgdGV4dC1hbGlnbjogY2VudGVyOyAiPjxkaXYgc3R5bGU9ImRpc3BsYXk6IGlubGluZS1ibG9jazsgZm9udC1zaXplOiAxMXB4OyBmb250LWZhbWlseTogVmVyZGFuYTsgY29sb3I6ICMwMDAwMDA7IGxpbmUtaGVpZ2h0OiAxLjI7IHBvaW50ZXItZXZlbnRzOiBhbGw7IHdoaXRlLXNwYWNlOiBub3JtYWw7IHdvcmQtd3JhcDogbm9ybWFsOyAiPjxiPjxmb250IGNvbG9yPSIjMDAwMGZmIj5BcHBlbCBkJ29mZnJlPC9mb250PjwvYj48L2Rpdj48L2Rpdj48L2Rpdj48L2ZvcmVpZ25PYmplY3Q+PHRleHQgeD0iMTIxIiB5PSIxODQiIGZpbGw9IiMwMDAwMDAiIGZvbnQtZmFtaWx5PSJWZXJkYW5hIiBmb250LXNpemU9IjExcHgiIHRleHQtYW5jaG9yPSJtaWRkbGUiPkFwcGVsIGQnb2ZmcmU8L3RleHQ+PC9zd2l0Y2g+PC9nPjxwYXRoIGQ9Ik0gMjAxIDI2MSBMIDIyMSAyNjEgTCAyMjEgMTA3LjM3IiBmaWxsPSJub25lIiBzdHJva2U9IiNkMWQxZDEiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9InN0cm9rZSIvPjxwYXRoIGQ9Ik0gMjIxIDEwMi4xMiBMIDIyNC41IDEwOS4xMiBMIDIyMSAxMDcuMzcgTCAyMTcuNSAxMDkuMTIgWiIgZmlsbD0iI2QxZDFkMSIgc3Ryb2tlPSIjZDFkMWQxIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJhbGwiLz48cmVjdCB4PSI0MSIgeT0iMjQxIiB3aWR0aD0iMTYwIiBoZWlnaHQ9IjQwIiByeD0iNiIgcnk9IjYiIGZpbGw9IiNmZmZmZmYiIHN0cm9rZT0iI2M0YzRjNCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiAxNThweDsgaGVpZ2h0OiAxcHg7IHBhZGRpbmctdG9wOiAyNjFweDsgbWFyZ2luLWxlZnQ6IDQycHg7Ij48ZGl2IHN0eWxlPSJib3gtc2l6aW5nOiBib3JkZXItYm94OyBmb250LXNpemU6IDA7IHRleHQtYWxpZ246IGNlbnRlcjsgIj48ZGl2IHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogMTFweDsgZm9udC1mYW1pbHk6IFZlcmRhbmE7IGNvbG9yOiAjMDAwMDAwOyBsaW5lLWhlaWdodDogMS4yOyBwb2ludGVyLWV2ZW50czogYWxsOyB3aGl0ZS1zcGFjZTogbm9ybWFsOyB3b3JkLXdyYXA6IG5vcm1hbDsgIj5EZXZpcyBmb3Vybmlzc2V1cjwvZGl2PjwvZGl2PjwvZGl2PjwvZm9yZWlnbk9iamVjdD48dGV4dCB4PSIxMjEiIHk9IjI2NCIgZmlsbD0iIzAwMDAwMCIgZm9udC1mYW1pbHk9IlZlcmRhbmEiIGZvbnQtc2l6ZT0iMTFweCIgdGV4dC1hbmNob3I9Im1pZGRsZSI+RGV2aXMgZm91cm5pc3NldXI8L3RleHQ+PC9zd2l0Y2g+PC9nPjxwYXRoIGQ9Ik0gMzIxIDEyMSBMIDMyMSAxNTQuNjMiIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2QxZDFkMSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSAzMjEgMTU5Ljg4IEwgMzE3LjUgMTUyLjg4IEwgMzIxIDE1NC42MyBMIDMyNC41IDE1Mi44OCBaIiBmaWxsPSIjZDFkMWQxIiBzdHJva2U9IiNkMWQxZDEiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxwYXRoIGQ9Ik0gNDAxIDEwMSBMIDQyMSAxMDEgTCA0MjEgMTQxIEwgNDM0LjYzIDE0MSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZDFkMWQxIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJzdHJva2UiLz48cGF0aCBkPSJNIDQzOS44OCAxNDEgTCA0MzIuODggMTQ0LjUgTCA0MzQuNjMgMTQxIEwgNDMyLjg4IDEzNy41IFoiIGZpbGw9IiNkMWQxZDEiIHN0cm9rZT0iI2QxZDFkMSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PHBhdGggZD0iTSA0MDEgMTAxIEwgNDIxIDEwMSBMIDQyMSA2MSBMIDQzNC42MyA2MSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZDFkMWQxIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJzdHJva2UiLz48cGF0aCBkPSJNIDQzOS44OCA2MSBMIDQzMi44OCA2NC41IEwgNDM0LjYzIDYxIEwgNDMyLjg4IDU3LjUgWiIgZmlsbD0iI2QxZDFkMSIgc3Ryb2tlPSIjZDFkMWQxIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJhbGwiLz48cmVjdCB4PSIyNDEiIHk9IjgxIiB3aWR0aD0iMTYwIiBoZWlnaHQ9IjQwIiByeD0iNiIgcnk9IjYiIGZpbGw9IiNmZmZmZmYiIHN0cm9rZT0iI2M0YzRjNCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiAxNThweDsgaGVpZ2h0OiAxcHg7IHBhZGRpbmctdG9wOiAxMDFweDsgbWFyZ2luLWxlZnQ6IDI0MnB4OyI+PGRpdiBzdHlsZT0iYm94LXNpemluZzogYm9yZGVyLWJveDsgZm9udC1zaXplOiAwOyB0ZXh0LWFsaWduOiBjZW50ZXI7ICI+PGRpdiBzdHlsZT0iZGlzcGxheTogaW5saW5lLWJsb2NrOyBmb250LXNpemU6IDExcHg7IGZvbnQtZmFtaWx5OiBWZXJkYW5hOyBjb2xvcjogIzAwMDAwMDsgbGluZS1oZWlnaHQ6IDEuMjsgcG9pbnRlci1ldmVudHM6IGFsbDsgd2hpdGUtc3BhY2U6IG5vcm1hbDsgd29yZC13cmFwOiBub3JtYWw7ICI+PHNwYW4gc3R5bGU9ImZvbnQtc2l6ZTogMTFweCI+PGZvbnQgc3R5bGU9ImZvbnQtc2l6ZTogMTFweCI+Q29tbWFuZGUgZm91cm5pc3NldXI8L2ZvbnQ+PC9zcGFuPjwvZGl2PjwvZGl2PjwvZGl2PjwvZm9yZWlnbk9iamVjdD48dGV4dCB4PSIzMjEiIHk9IjEwNCIgZmlsbD0iIzAwMDAwMCIgZm9udC1mYW1pbHk9IlZlcmRhbmEiIGZvbnQtc2l6ZT0iMTFweCIgdGV4dC1hbmNob3I9Im1pZGRsZSI+Q29tbWFuZGUgZm91cm5pc3NldXI8L3RleHQ+PC9zd2l0Y2g+PC9nPjxyZWN0IHg9IjI0MSIgeT0iMTYxIiB3aWR0aD0iMTYwIiBoZWlnaHQ9IjQwIiByeD0iNiIgcnk9IjYiIGZpbGw9IiNmZmZmZmYiIHN0cm9rZT0iI2M0YzRjNCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiAxNThweDsgaGVpZ2h0OiAxcHg7IHBhZGRpbmctdG9wOiAxODFweDsgbWFyZ2luLWxlZnQ6IDI0MnB4OyI+PGRpdiBzdHlsZT0iYm94LXNpemluZzogYm9yZGVyLWJveDsgZm9udC1zaXplOiAwOyB0ZXh0LWFsaWduOiBjZW50ZXI7ICI+PGRpdiBzdHlsZT0iZGlzcGxheTogaW5saW5lLWJsb2NrOyBmb250LXNpemU6IDExcHg7IGZvbnQtZmFtaWx5OiBWZXJkYW5hOyBjb2xvcjogIzAwMDAwMDsgbGluZS1oZWlnaHQ6IDEuMjsgcG9pbnRlci1ldmVudHM6IGFsbDsgd2hpdGUtc3BhY2U6IG5vcm1hbDsgd29yZC13cmFwOiBub3JtYWw7ICI+w4ljcml0dXJlIGRlIHBhaWVtZW50PGJyIHN0eWxlPSJmb250LXNpemU6IDExcHgiIC8+KEFjb21wdGUpPC9kaXY+PC9kaXY+PC9kaXY+PC9mb3JlaWduT2JqZWN0Pjx0ZXh0IHg9IjMyMSIgeT0iMTg0IiBmaWxsPSIjMDAwMDAwIiBmb250LWZhbWlseT0iVmVyZGFuYSIgZm9udC1zaXplPSIxMXB4IiB0ZXh0LWFuY2hvcj0ibWlkZGxlIj7DiWNyaXR1cmUgZGUgcGFpZW1lbnQuLi48L3RleHQ+PC9zd2l0Y2g+PC9nPjxyZWN0IHg9IjQ0MSIgeT0iNDEiIHdpZHRoPSIxNjAiIGhlaWdodD0iNDAiIHJ4PSI2IiByeT0iNiIgZmlsbD0iI2ZmZmZmZiIgc3Ryb2tlPSIjYzRjNGM0IiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSAtMC41KSI+PHN3aXRjaD48Zm9yZWlnbk9iamVjdCBzdHlsZT0ib3ZlcmZsb3c6IHZpc2libGU7IHRleHQtYWxpZ246IGxlZnQ7IiBwb2ludGVyLWV2ZW50cz0ibm9uZSIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ij48ZGl2IHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sIiBzdHlsZT0iZGlzcGxheTogZmxleDsgYWxpZ24taXRlbXM6IHVuc2FmZSBjZW50ZXI7IGp1c3RpZnktY29udGVudDogdW5zYWZlIGNlbnRlcjsgd2lkdGg6IDE1OHB4OyBoZWlnaHQ6IDFweDsgcGFkZGluZy10b3A6IDYxcHg7IG1hcmdpbi1sZWZ0OiA0NDJweDsiPjxkaXYgc3R5bGU9ImJveC1zaXppbmc6IGJvcmRlci1ib3g7IGZvbnQtc2l6ZTogMDsgdGV4dC1hbGlnbjogY2VudGVyOyAiPjxkaXYgc3R5bGU9ImRpc3BsYXk6IGlubGluZS1ibG9jazsgZm9udC1zaXplOiAxMXB4OyBmb250LWZhbWlseTogVmVyZGFuYTsgY29sb3I6ICMwMDAwMDA7IGxpbmUtaGVpZ2h0OiAxLjI7IHBvaW50ZXItZXZlbnRzOiBhbGw7IHdoaXRlLXNwYWNlOiBub3JtYWw7IHdvcmQtd3JhcDogbm9ybWFsOyAiPlJlw6d1IGQnYWNoYXQ8L2Rpdj48L2Rpdj48L2Rpdj48L2ZvcmVpZ25PYmplY3Q+PHRleHQgeD0iNTIxIiB5PSI2NCIgZmlsbD0iIzAwMDAwMCIgZm9udC1mYW1pbHk9IlZlcmRhbmEiIGZvbnQtc2l6ZT0iMTFweCIgdGV4dC1hbmNob3I9Im1pZGRsZSI+UmXDp3UgZCdhY2hhdDwvdGV4dD48L3N3aXRjaD48L2c+PHBhdGggZD0iTSA2MDEgMTQxIEwgNjM0LjYzIDE0MSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZDFkMWQxIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJzdHJva2UiLz48cGF0aCBkPSJNIDYzOS44OCAxNDEgTCA2MzIuODggMTQ0LjUgTCA2MzQuNjMgMTQxIEwgNjMyLjg4IDEzNy41IFoiIGZpbGw9IiNkMWQxZDEiIHN0cm9rZT0iI2QxZDFkMSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PHJlY3QgeD0iNDQxIiB5PSIxMjEiIHdpZHRoPSIxNjAiIGhlaWdodD0iNDAiIHJ4PSI2IiByeT0iNiIgZmlsbD0iI2ZmZmZmZiIgc3Ryb2tlPSIjYzRjNGM0IiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSAtMC41KSI+PHN3aXRjaD48Zm9yZWlnbk9iamVjdCBzdHlsZT0ib3ZlcmZsb3c6IHZpc2libGU7IHRleHQtYWxpZ246IGxlZnQ7IiBwb2ludGVyLWV2ZW50cz0ibm9uZSIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ij48ZGl2IHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sIiBzdHlsZT0iZGlzcGxheTogZmxleDsgYWxpZ24taXRlbXM6IHVuc2FmZSBjZW50ZXI7IGp1c3RpZnktY29udGVudDogdW5zYWZlIGNlbnRlcjsgd2lkdGg6IDE1OHB4OyBoZWlnaHQ6IDFweDsgcGFkZGluZy10b3A6IDE0MXB4OyBtYXJnaW4tbGVmdDogNDQycHg7Ij48ZGl2IHN0eWxlPSJib3gtc2l6aW5nOiBib3JkZXItYm94OyBmb250LXNpemU6IDA7IHRleHQtYWxpZ246IGNlbnRlcjsgIj48ZGl2IHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogMTFweDsgZm9udC1mYW1pbHk6IFZlcmRhbmE7IGNvbG9yOiAjMDAwMDAwOyBsaW5lLWhlaWdodDogMS4yOyBwb2ludGVyLWV2ZW50czogYWxsOyB3aGl0ZS1zcGFjZTogbm9ybWFsOyB3b3JkLXdyYXA6IG5vcm1hbDsgIj5GYWN0dXJlIGQnYWNoYXQ8L2Rpdj48L2Rpdj48L2Rpdj48L2ZvcmVpZ25PYmplY3Q+PHRleHQgeD0iNTIxIiB5PSIxNDQiIGZpbGw9IiMwMDAwMDAiIGZvbnQtZmFtaWx5PSJWZXJkYW5hIiBmb250LXNpemU9IjExcHgiIHRleHQtYW5jaG9yPSJtaWRkbGUiPkZhY3R1cmUgZCdhY2hhdDwvdGV4dD48L3N3aXRjaD48L2c+PHJlY3QgeD0iNjQxIiB5PSIxMjEiIHdpZHRoPSIxNjAiIGhlaWdodD0iNDAiIHJ4PSI2IiByeT0iNiIgZmlsbD0iI2ZmZmZmZiIgc3Ryb2tlPSIjYzRjNGM0IiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSAtMC41KSI+PHN3aXRjaD48Zm9yZWlnbk9iamVjdCBzdHlsZT0ib3ZlcmZsb3c6IHZpc2libGU7IHRleHQtYWxpZ246IGxlZnQ7IiBwb2ludGVyLWV2ZW50cz0ibm9uZSIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ij48ZGl2IHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sIiBzdHlsZT0iZGlzcGxheTogZmxleDsgYWxpZ24taXRlbXM6IHVuc2FmZSBjZW50ZXI7IGp1c3RpZnktY29udGVudDogdW5zYWZlIGNlbnRlcjsgd2lkdGg6IDE1OHB4OyBoZWlnaHQ6IDFweDsgcGFkZGluZy10b3A6IDE0MXB4OyBtYXJnaW4tbGVmdDogNjQycHg7Ij48ZGl2IHN0eWxlPSJib3gtc2l6aW5nOiBib3JkZXItYm94OyBmb250LXNpemU6IDA7IHRleHQtYWxpZ246IGNlbnRlcjsgIj48ZGl2IHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogMTFweDsgZm9udC1mYW1pbHk6IFZlcmRhbmE7IGNvbG9yOiAjMDAwMDAwOyBsaW5lLWhlaWdodDogMS4yOyBwb2ludGVyLWV2ZW50czogYWxsOyB3aGl0ZS1zcGFjZTogbm9ybWFsOyB3b3JkLXdyYXA6IG5vcm1hbDsgIj7DiWNyaXR1cmUgZGUgcGFpZW1lbnQ8L2Rpdj48L2Rpdj48L2Rpdj48L2ZvcmVpZ25PYmplY3Q+PHRleHQgeD0iNzIxIiB5PSIxNDQiIGZpbGw9IiMwMDAwMDAiIGZvbnQtZmFtaWx5PSJWZXJkYW5hIiBmb250LXNpemU9IjExcHgiIHRleHQtYW5jaG9yPSJtaWRkbGUiPsOJY3JpdHVyZSBkZSBwYWllbWVudDwvdGV4dD48L3N3aXRjaD48L2c+PC9nPjxzd2l0Y2g+PGcgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ii8+PGEgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtNSkiIHhsaW5rOmhyZWY9Imh0dHBzOi8vd3d3LmRpYWdyYW1zLm5ldC9kb2MvZmFxL3N2Zy1leHBvcnQtdGV4dC1wcm9ibGVtcyIgdGFyZ2V0PSJfYmxhbmsiPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIGZvbnQtc2l6ZT0iMTBweCIgeD0iNTAlIiB5PSIxMDAlIj5WaWV3ZXIgZG9lcyBub3Qgc3VwcG9ydCBmdWxsIFNWRyAxLjE8L3RleHQ+PC9hPjwvc3dpdGNoPjwvc3ZnPg==
```

---

Pour accéder à l'**appel d'offre**, allez sur:

> Accueil > Achats > **Appel d'offre**

![appel_d'offre_liste.png](/buying/request-for-quotation/appel_d'offre_liste.png)

## 1. Prérequis

Avant de créer et d'utiliser un appel d'offre, il est conseillé de créer d'abord les éléments suivants:

1. **[Un fournisseur](/fr/buying/supplier)**
2. **[Un article](/fr/stocks/item)**

## 2. Comment créer un appel d'offre

- Accédez à la liste **Appel d'offre**, cliquez sur **:heavy_plus_sign:Ajouter un Appel d'offre**.
- Entrez la **date**.
- Le **statut** : Il évoluera automatiquement lors de l'enregistrement de l'offre puis lors de la validation, ou annulation.
- Choisissez le **fournisseur** : Si toutes les informations "Contact" et "Email ont bien été enregistrés sur la fiche du fournisseur, elles seront renseignés automatiquement.
- Dans le tableau "**Articles**", saisissez les articles, la quantité, l'unité de mesure et l'entrepôt cible où vous enverrez les articles.
- L'entrepôt peut être laissé vide si «Maintenir le stock» n'est pas coché pour l'article.

![création_appel_d'offre.png](/buying/request-for-quotation/création_appel_d'offre.png)

## 3. Caractéristiques

### 3.1 Obtenir des élements

Les éléments de la table des éléments peuvent être extraits par d'autres documents qui sont : 
- **Demande d'article**
- **Opportunité**
- **Fournisseur**

**Demande de matériel** : les articles seront récupérés à partir d'une demande de matériel soumise que vous sélectionnez. Une demande de matériel peut être recherchée avec certains mots correspondants et une plage de dates peut également être sélectionnée pour filtrer les demandes de matériel.

**Opportunité** : les éléments seront récupérés à partir d'une opportunité enregistrée. Une plage de dates peut également être sélectionnée ici.

**Fournisseur potentiel** : Sélectionnez un fournisseur possible. Ensuite, si vous avez soumis des demandes de matériel auprès de ce fournisseur, les articles peuvent être récupérés à partir de celui-ci.

![obtenir_les_articles_de_-_appel_d'offre.png](/buying/request-for-quotation/obtenir_les_articles_de_-_appel_d'offre.png)

### 3.2 Obtenir des fournisseurs

Au lieu de saisir manuellement les fournisseurs dans le tableau, vous pouvez également les récupérer à l'aide du bouton **Obtenir des fournisseurs**. Lorsque vous cliquez sur Outils > **Obtenir des fournisseurs** , vous verrez le champ **Obtenir des fournisseurs par**. Il existe deux options pour récupérer les fournisseurs : 
- Par Tag
- Par groupe.

**Par tag** : accédez à "Catégorie de tag" en effectuant une recherche dans la barre de recherche. Vous devez d'abord avoir créé des balises ici et les avoir attribuées à un fournisseur dans le module d'achat. Ensuite, vous pouvez sélectionner «Par tag». En cliquant sur Ajouter «Tous les fournisseurs», les fournisseurs avec des balises correspondantes seront récupérés.

**Par groupe** : sélectionnez «Groupe de fournisseurs» et choisissez le groupe de fournisseurs à partir duquel les fournisseurs doivent être ajoutés. Par exemple, si vous sélectionnez Matériel, tous vos fournisseurs de matériel seront ajoutés afin que vous puissiez obtenir un devis de tous.

Dans le tableau des fournisseurs, en développant une ligne avec le triangle inversé, vous verrez une option «I le PDF» qui ouvrira un PDF de l'appel d'offres.

![obtenir_des_fournisseurs_-_appel_d'offre.png](/buying/request-for-quotation/obtenir_des_fournisseurs_-_appel_d'offre.png)

### 3.3 Lien vers les demandes de matériel

Lorsque vous cliquez sur Outils > **Lien vers les demandes de matériel**, cela lie l'appel d'offre aux demandes de matériel disponibles. Les articles doivent être les mêmes dans la demande de devis et la demande de matériel.

Désormais, lorsque l'appel d'offre est enregistré, vous pouvez voir dans le tableau de bord qu'elle est liée à la demande de matériel. S'il y a plusieurs demandes de matériel avec les mêmes articles, le lien sera créé avec la demande de matériel la plus récente.

![lien_vers_demande_de_matériel_-_appel_d'offre.png](/buying/request-for-quotation/lien_vers_demande_de_matériel_-_appel_d'offre.png)

### 3.4 Aperçu des e-mails

Dans la section «Détails de l'e-mail», d'un projet d'appel d'offre, il existe une disposition permettant de créer et de prévisualiser votre e-mail à envoyer au fournisseur.

Saisissez les éventuels messages supplémentaires pour le fournisseur dans le champ «Message pour le fournisseur». Ce champ peut être rempli automatiquement à l'aide du champ «Modèle d'e-mail».

Une salutation peut être ajoutée et le champ «Objet» peut également être modifié. Une fois terminé, vous pouvez cliquer sur le bouton **Aperçu de l'e-mail** et voir un aperçu de l'e-mail qui sera envoyé.

### 3.5 Termes et conditions

Dans les transactions de vente / achat, il peut y avoir certaines conditions générales sur la base desquelles le fournisseur fournit des biens ou des services au client. Vous pouvez appliquer les conditions générales aux transactions et elles apparaîtront lors de l'impression du document.

### 3.6 Paramètres d'impression

- **En-tête de lettre**
Vous pouvez imprimer votre demande de devis / bon de commande sur le papier à en-tête de votre entreprise. En savoir plus ici .

«Regrouper les mêmes éléments» regroupera les mêmes éléments ajoutés plusieurs fois dans le tableau des éléments. Cela peut être vu lorsque votre impression.

- **Imprimer les titres** 
Les titres de vos documents peuvent être modifiés.

## 4. Création d'un devis fournisseur après l'appel d'offres 

Après la création de l'appel d'offre, il existe deux façons de générer un devis fournisseur.

### 4.1 Devis fournisseur par l'utilisateur

1. Ouvrez l'appel d'offre et cliquez sur Devis fournisseur> Créer.

- Sélectionnez le fournisseur, cliquez à nouveau sur le fournisseur. Dans cette page, cliquez sur le + à côté de «Devis fournisseur». Une nouvelle page de devis fournisseur sera ouverte, l'utilisateur doit entrer la quantité, le taux et le soumettre.


### 4.2 Devis fournisseur par le fournisseur

1. Si un contact est créé pour le fournisseur et qu'une adresse e-mail est associée au contact, les détails du contact et l'adresse e-mail seront récupérés lors de la sélection du fournisseur. Créez un contact et une adresse e-mail s'ils ne sont pas déjà présents.

2. Cliquez sur Outils > **Envoyer des e-mails aux fournisseurs**. 

- Si le compte du fournisseur n'est pas présent : le système créera le compte du fournisseur et enverra les détails au fournisseur. Le fournisseur devra cliquer sur le lien (Password Update) présent dans l'email. Après la mise à jour du mot de passe, le Fournisseur peut accéder à son portail avec le formulaire **Appel d'offre**. Le fournisseur sera créé en tant qu'utilisateur du site Web.

- Si le compte du fournisseur est présent : le système enverra un lien d'appel d'offre au fournisseur. Le fournisseur doit se connecter en utilisant ses identifiants pour consulter le formulaire d'appel d'offre sur le portail.

3. Dans tous les cas, lorsque le fournisseur se connecte, l'écran suivant lui est affiché. De là, ils peuvent vous envoyer un devis:

- Le fournisseur doit saisir le montant et les notes (conditions de paiement) sur le formulaire et cliquer sur Soumettre. Dans la section Devis, les devis précédents seront visibles.

4. Lors de la soumission, Dokos créera un devis fournisseur (mode brouillon) contre le fournisseur. L'utilisateur doit examiner le devis du fournisseur et le soumettre. Lorsque tous les articles de l'appel d'offre s ont été proposés par un fournisseur, le statut du devis est mis à jour sur **Reçu** dans le tableau **Fournisseurs** de l'appel d'offre.

