---
title: Fournisseur
description: 
published: true
date: 2021-05-08T08:34:55.960Z
tags: 
editor: markdown
dateCreated: 2020-11-26T17:04:23.195Z
---

# Création d'un fournisseur

Un fournisseur est une personne physique ou morale auprès de qui vous achetez des produits ou des services.

## 1. Informations sur les fournisseurs
> Achats > **Fournisseur**

Un fournisseur est une donnée de base, ne nécessitant aucun pré-requis.

### 1.1 Réglages par défaut

Afin de faciliter la création de vos fournisseurs, il est possible de définir certaines informations par défaut dans les **paramètres d'achat**.

1. **Système de nommage**: Chaque fournisseur possède un identifiant unique, qui est le nom du document dans lequel sont renseignés les informations du fournisseur. Cet identifiant du fournisseur peut être basé sur le nom complet du fournisseur ou sur un numéro de série.  
Si vous choisissez de le baser sur le nom complet du fournisseur, en cas d'homonymes, _Dokos_ rajoutera automatiquement un "-1" à la fin du nom pour les différencier.  
Si vous choisissez de le baser sur une série, vous pouvez définir le préfixe de cette série dans le type de document **Nom de série**. 

2. **Groupe de fournisseurs par défaut**: Définissez le groupe de fournisseurs par défaut. Il s'agit généralement du groupe de fournisseurs le plus souvent sélectionné.

3. **Liste de prix d'achat par défaut**: Définissez la liste de prix qui sera sélectionnée par défaut dans lorsque vous créez un nouveau fournisseur.

### 1.2 Fonctionnalités

Les informations renseignées dans la fiche fournisseur servent ensuite de valeurs par défaut dans les transactions.
Par exemple, si vous mettez "USD" comme devise de facturation dans la fiche du fournisseur, les documents générés pour ce fournisseur (commande, factures, ...) seront automatiquement en US Dollars. Vous pouvez toujours modifier cette valeur dans le document correspondant au moment de sa création.

## 2. Comment accéder à la liste des fournisseurs ?

Pour accéder à la liste des fournisseurs, accédez à:

> Accueil > Achats > Fournisseur

### 2.1 Comment ajouter un fournisseur ?

- Allez sur la liste des fournisseurs
- Cliquez en haut à droite sur **:heavy_plus_sign:Ajouter Fournisseur** 
> Une fenêtre modale (ou Pop-up) s’affiche 
{.is-info}

![liste_fournisseur.png](/buying/liste_fournisseur.png)

### 2.2 Création de la fiche fournisseur - *Ajout rapide*

- Saisissez le **Nom du fournisseur**
- Sélectionner le **Groupe de fournisseurs**
- Indiquez le **Type de fournisseur** (Choix entre SOCIÉTÉ ou INDIVIDUEL)
- Si vous cocher la **case Désactivé**, le fournisseur sera masqué de la liste des fournisseurs
- Cliquez sur **Enregistrer**

![ajout_rapide_fournisseur.png](/buying/ajout_rapide_fournisseur.png)

### 2.3 Création de la fiche fournisseur - *Complet*
- Cliquez sur "**Modifier en pleine page**"

Une fois arrivée sur la page "**Nouveau.elle. Fournisseur**", vous pouvez créer complètement votre fiche fournisseur.

![ajout_complet_fournisseur.png](/buying/ajout_complet_fournisseur.png)

#### 2.3.1 Détails de la fiche fournisseur

- **Nom du fournisseur** : Nom de la personne morale ou personne physique
- **Groupe de fournisseurs** (Sélectionnez selon la liste **Groupe de fournisseurs**. *Exemples : Distributeurs, Matières premières, Services...*)
- **Pays** : Lieu domiciliation du fournisseur (*France, Italie, Allemagne...*)
- **Type de fournisseur** (*Choix entre SOCIÉTÉ ou INDIVIDUEL*)
- **Compte bancaire par défaut** : Sélectionner le compte par défaut du fournisseur. Pour créer le compte bancaire, cliquez sur la case et faites Créer un nouveau compte bancaire
- **Numéro de compte permanent** (PAN)
- **Numéro de TVA**: Renseigner le numéro d’identification fiscale de votre fournisseur
- **Catégorie de taxe** : Ceci est lié à la règle fiscale . Si une catégorie de taxe est définie ici, lorsque vous sélectionnez ce fournisseur, le modèle de taxe d'achat et de frais correspondant sera appliqué. Ce modèle est lié à la règle de taxe et la règle de taxe est liée à une catégorie de taxe. La catégorie de taxe peut être utilisée pour regrouper les fournisseurs auxquels la même taxe sera appliquée. Par exemple: gouvernement, commercial, etc.
- **Catégorie de taxation à la source**
- **Est Transporteur** : Si le fournisseur vend vos services de transport, cochez cette case. Le champ "GST Transporter ID" sera visible si ce champ est coché.
- **Fournisseur interne** : Si le fournisseur fait partie d'une société sœur ou d'une société mère/enfant, cochez ce champ et sélectionnez la société qu'il représente.

#### 2.3.2 Autoriser la création d'une facture d'achat sans bon de commande ni reçu d'achat

Si l'option «**Commande fournisseur requis**» ou "**Reçu d'achat requis**» est configurée sur "Oui" dans les paramètres d'achat , elle peut être remplacée pour un fournisseur particulier en activant l'option "**Autoriser la création de la facture d'achat sans commande fournisseur**" ou "**Autoriser la création de la facture d'achat» Sans reçu d'achat"** dans la fiche fournisseur.

![autres_champs_fournisseur.png](/buying/autres_champs_fournisseur.png)


#### 2.3.3 Devise et liste de prix

- **Devise de facturation** : la devise de votre fournisseur peut être différente de la devise de votre entreprise. Si vous choisissez EUR pour un fournisseur, la devise sera remplie en EUR et le taux de change sera indiqué pour les transactions d'achat futures.

- **Liste de prix** : Chaque fournisseur peut avoir une liste de prix par défaut afin que chaque fois que vous achetez un nouveau produit/service auprès de ce fournisseur à des prix différents et afin que la liste de prix associée au fournisseur soit également mise à jour. Sous la liste de prix vient le prix de l'article, vous pouvez voir les prix dans Achat > Articles et prix > **Prix de l'article**.

![liste_de_prix_fournisseur.png](/buying/liste_de_prix_fournisseur.png)

Si vous sélectionnez un fournisseur particulier, la liste de prix associée sera récupérée dans les transactions d'achat.

#### 2.3.4 Modèle de termes de paiement par défaut

- **Limite de crédit**: si un modèle de conditions de paiement est défini ici, il sera automatiquement sélectionné pour les futures transactions d'achat.

- **Bloquer le fournisseur** : vous pouvez bloquer les factures, les paiements ou les deux d'un fournisseur jusqu'à une date précise. Choisissez «Type de suspension», si vous ne sélectionnez pas de type de suspension, ERPNext le définira sur «Tout». Lorsqu'un fournisseur est bloqué, son statut sera affiché comme «En attente».

**Les types de blocage sont les suivants :**
- **Factures** : DOKOS n'autorisera pas la création de factures d'achat ou de bons de commande pour le fournisseur
- **Paiements** : DOKOS ne permettra pas la création d'entrées de paiement pour le fournisseur
Tous : DOKOS appliquera les deux types de blocage ci-dessus.

Si vous ne définissez pas de date de sortie, DOKOS conservera le fournisseur indéfiniment .

#### 2.3.5 Compte fournisseur par défaut

Ajoutez le compte par défaut à partir duquel les factures de ce fournisseur seront payées. Ajoutez des lignes supplémentaires pour plus d'entreprises, vous ne pouvez sélectionner qu'un seul compte par entreprise.

Vous pouvez intégrer un fournisseur avec un compte. Pour tous les fournisseurs, le compte "**Créancier**" est **défini comme compte à payer par défaut**. Lorsque la facture d'achat est créée et donc payable envers le fournisseur, elle est imputée au compte "Créanciers".

Si vous souhaitez personnaliser le compte fournisseur pour le fournisseur, vous devez d'abord ajouter un compte fournisseur dans le **plan comptable**, puis sélectionner ce compte fournisseur dans la fiche fournisseur.

Si vous ne souhaitez pas personnaliser le compte fournisseur et continuer avec le compte fournisseur par défaut «Créancier», **n’indiquez pas de valeur dans le tableau du compte fournisseur par défaut**.

**Conseil** : le compte fournisseur par défaut est défini dans la fiche de la société. Si vous souhaitez définir un autre compte comme compte par défaut pour le compte à payer au lieu du compte de créancier, accédez à la fiche de la société et définissez ce compte comme "compte à payer par défaut".

En fonction de votre plan, vous pouvez ajouter plusieurs sociétés dans votre instance DOKOS. Un fournisseur peut être utilisé dans plusieurs entreprises. Dans ce cas, vous devez définir le compte fournisseur au niveau de l'entreprise pour le fournisseur dans le tableau "Comptes fournisseurs par défaut", c'est-à-dire ajouter plusieurs lignes.

![compte_fournisseur_par_defaut.png](/buying/compte_fournisseur_par_defaut.png)

#### 2.3.6 Informations additionnelles

Vous pouvez ajouter des informations facultatives sur la fiche du fournisseur :

- **Le site web** du fournisseur
- **Les détails supplémentaires** sur le fournisseur
- **La langue d’impression** des documents. Par défaut sur la langue paramétrée dans Paramètre 
- **Est gelé**, si vous cochez la case Est gelé, les écritures comptables pour le fournisseur seront gelées. Dans ce cas, le seul utilisateur dont les entrées dépasseront le "gel" est le rôle attribué dans Rôle autorisé à définir des comptes gelés et à modifier les entrées gelées dans Comptabilité> Paramètres> Paramètres des comptes. Ceci est utile lorsque le nom du fournisseur ou les coordonnées bancaires sont modifiés.

## 3. Après l’enregistrement du fournisseur

Une fois que tous les détails nécessaires sont remplis, enregistrez le document. Lors de l'enregistrement, les options permettant de créer les éléments suivants seront visibles dans le tableau de bord :

- **[Appel d'offre](/fr/buying/request-for-quotation)** : Effectuer un demande un devis chez ce fournisseur.
- **[Devis fournisseur](/fr/buying/supplier-quote)** : Toutes les offres que le fournisseur vous a envoyées et que vous avez soumises dans le système.
- **[Commande fournisseur](/fr/buying/order-supplier)** : Bons de commande que vous avez passés auprès de ce fournisseur.
- **[Reçu d'achat](/fr/stocks/purchase-receipt)** : Les reçus d'achat fournis par ce fournisseur que vous avez enregistrés dans le système.
- **[Facture d'achat](/fr/accounting/purchase-invoice)** : Factures d'achat que vous avez effectuées auprès de ce fournisseur.
- **[Saisie de paiement](/fr/accounting/payment-entry)** : Ecritures de paiement pour les factures d'achat contre ce fournisseur.
- **[Prix des articles](/fr/stocks/price-rules)**: Toutes les règles de tarification liées à ce fournisseur.