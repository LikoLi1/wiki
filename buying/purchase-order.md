---
title: Commande Fournisseur
description: 
published: true
date: 2021-05-20T10:20:05.665Z
tags: commande fournisseur
editor: markdown
dateCreated: 2021-05-05T09:52:15.123Z
---

# Commande fournisseur
Une commande fournisseur est un contrat conclu avec votre fournisseur selon lequel vous vous engagez à acheter un ensemble d'articles dans des conditions données.

Il est similaire à une commande client, mais au lieu de l'envoyer à une partie externe, vous le conservez pour les enregistrements internes.


```diagram
PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSI4NDJweCIgaGVpZ2h0PSIzMjJweCIgdmlld0JveD0iLTAuNSAtMC41IDg0MiAzMjIiIGNvbnRlbnQ9IiZsdDtteGZpbGUgaG9zdD0mcXVvdDtlbWJlZC5kaWFncmFtcy5uZXQmcXVvdDsgbW9kaWZpZWQ9JnF1b3Q7MjAyMS0wNS0wN1QwODo0OTo1Mi43NDRaJnF1b3Q7IGFnZW50PSZxdW90OzUuMCAoTWFjaW50b3NoOyBJbnRlbCBNYWMgT1MgWCAxMF8xNV83KSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvOTAuMC40NDMwLjkzIFNhZmFyaS81MzcuMzYmcXVvdDsgZXRhZz0mcXVvdDtXdm1iNk5qWnc4TllxRlBwUk05YyZxdW90OyB2ZXJzaW9uPSZxdW90OzE0LjYuMTAmcXVvdDsgdHlwZT0mcXVvdDtlbWJlZCZxdW90OyZndDsmbHQ7ZGlhZ3JhbSBpZD0mcXVvdDtIbGk3a0ZrNS10XzJIYkFzQWxJLSZxdW90OyBuYW1lPSZxdW90O1BhZ2UtMSZxdW90OyZndDs3VnBkYjlNd0ZQMDFGZkFBaXBPUXRZK2xYZUVCSk1Ra1BoNjk1RFl4YytMZ09HdkxyOGQyN0NacDJqRm8ybFN3VnRyaTQrdll2dWVlNjQ5dDVNM1M5VnVPOCtRRGk0Q09YQ2Rhajd6NXlIVW5maUIvS21CVEFhN25qeXNrNWlTcU1GUUROK1FuR05BeGFFa2lLRnFHZ2pFcVNONEdRNVpsRUlvV2hqbG5xN2Jaa3RGMnJ6bU9vUVBjaEpoMjBTOGtFb2xCa2VQVUZlK0F4SW50K3NyV3BOaGFHNkJJY01SV0RjaTdIbmt6enBpb250TDFES2p5bm5WTTFXNXhvSFk3TWc2WmVFd0R0MnB3ajJscEptZkdKVFoydGhESnlac2k0eUpoTWNzd3ZhN1JONXlWV1FUcWpZNHMxVGJ2R2NzbGlDVDRIWVRZR0NaeEtaaUVFcEZTVTd0a21UQ1ZTSlVwdmdVcVh4UUJuekhLdUI2SU45RWZXVjBJenU2Z1VUTkg2bXRldE1BcG9TcTBQZ09QY0lZbDNQV0tjVlRCU2g2YWVmb21rakNQd1ZpWlFGVWVhRFF6bm53TExBWEJOOUtBQThXQzNMZkRBNXNvaTdkMk5RL3l3VkN4bnhidkltbnAyYnZJSGNxOWZzZTljMGl4ZEpiS1NsRHBkRFR6UnRNSkp5cDM3YmkrZHF6eXlpb2hBbTV5ckdlNmtobXY3Y1FxV0cyZVVFZ09uTWdoQTFlTlNCWWJnanBSUGZQVlY5WFlKS0hON2tDRWlTbkVGQmVGZVQ3QWppYlJqTjA1cURVYzNzVjZXcmIvakdYd0VMZjN3QVdzSDZUTjFJNU5xdHUwaTZ0RzVnd01salNTcHU4Y3ovUHJpNVRSYWRKWDBCWFllQ2g5QlIyL1QvTzgyZ1E4WThzbGh5ZEo5U21wclg3T29hbXJpOVNVS2pkNGR2VG5aRnJyWFZpbTZVZEdaSzlibXQxZ2gyZG5oNzlLN0taVlRlR1VjN3hwbU9YS29IaDhQN1pjUjBUMXhqbyt0bk44Vk1pTTl5eTM5NlRRVysrU1o2UW9vT1JQS2FIUGxMQ1YramxTd3VRaVU4SnBwRzgzcmEyTnJEZlVRbXRQeEJmbSt0NGQzUHRCN0sveXBIOGdIeThlYSsvM20xY1JHb0pzU1I3ZmZEWHRkZUdiS3J4NmJZdnpkYk55dmptUUVuc1BFbjh3RmU2N1JRbW9ITmViMnhZOXdZOVNYZXpvcWI4c3RDK20wZ0NoZksxbmJ1dmxVMngrVTJ1djc3TnNMcXNOOVZabnVXeENSM1E0WTZrOUI3ZVc1bW9ZMGcvVlNOcWprL0J0amYzTHEvaVNVTnJvOUhxc3Z2MnMzME1lazlHZTY2YVpOeHBQUWs1RXliZTNJamtta0lJT2dDcTQrVkhCOW53YXNqUVg4T0xmRHBxVGIvMEdQUTZpN2wzYUo5QjNaMWVsUHUvak1NSGlpZUdqR1BaM0dEN241aDc5VDVkb2FNOHRHcm9hYkYvUnZVZGI0TkJtNVBNcWF6ajlIQmIxem5LODBKL1RLQTY1NTVUY25qdTJoOWZqcCt4NkJOZkJvRngzTDhjNmRQNkd3S1lNRG1sbW14M2JiTHV0MUl4MkNNTThORVFFUGFtcTY5aXh2OGV4bnZ2bm5wWEYrcy9tMVdHNi91OEQ3L29YJmx0Oy9kaWFncmFtJmd0OyZsdDsvbXhmaWxlJmd0OyI+PGRlZnMvPjxnPjxwYXRoIGQ9Ik0gMTIxIDEyMSBMIDEyMSAxNTQuNjMiIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2QxZDFkMSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSAxMjEgMTU5Ljg4IEwgMTE3LjUgMTUyLjg4IEwgMTIxIDE1NC42MyBMIDEyNC41IDE1Mi44OCBaIiBmaWxsPSIjZDFkMWQxIiBzdHJva2U9IiNkMWQxZDEiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxwYXRoIGQ9Ik0gMjAxIDEwMSBMIDIzNC42MyAxMDEiIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzAwMDAwMCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSAyMzkuODggMTAxIEwgMjMyLjg4IDEwNC41IEwgMjM0LjYzIDEwMSBMIDIzMi44OCA5Ny41IFoiIGZpbGw9IiMwMDAwMDAiIHN0cm9rZT0iIzAwMDAwMCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PHJlY3QgeD0iNDEiIHk9IjgxIiB3aWR0aD0iMTYwIiBoZWlnaHQ9IjQwIiByeD0iNiIgcnk9IjYiIGZpbGw9IiNmZmZmZmYiIHN0cm9rZT0iI2M0YzRjNCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiAxNThweDsgaGVpZ2h0OiAxcHg7IHBhZGRpbmctdG9wOiAxMDFweDsgbWFyZ2luLWxlZnQ6IDQycHg7Ij48ZGl2IHN0eWxlPSJib3gtc2l6aW5nOiBib3JkZXItYm94OyBmb250LXNpemU6IDA7IHRleHQtYWxpZ246IGNlbnRlcjsgIj48ZGl2IHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogMTFweDsgZm9udC1mYW1pbHk6IFZlcmRhbmE7IGNvbG9yOiAjMDAwMDAwOyBsaW5lLWhlaWdodDogMS4yOyBwb2ludGVyLWV2ZW50czogYWxsOyB3aGl0ZS1zcGFjZTogbm9ybWFsOyB3b3JkLXdyYXA6IG5vcm1hbDsgIj5EZW1hbmRlIGRlIG1hdMOpcmllbDwvZGl2PjwvZGl2PjwvZGl2PjwvZm9yZWlnbk9iamVjdD48dGV4dCB4PSIxMjEiIHk9IjEwNCIgZmlsbD0iIzAwMDAwMCIgZm9udC1mYW1pbHk9IlZlcmRhbmEiIGZvbnQtc2l6ZT0iMTFweCIgdGV4dC1hbmNob3I9Im1pZGRsZSI+RGVtYW5kZSBkZSBtYXTDqXJpZWw8L3RleHQ+PC9zd2l0Y2g+PC9nPjxwYXRoIGQ9Ik0gMTIxIDIwMSBMIDEyMSAyMzQuNjMiIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2QxZDFkMSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSAxMjEgMjM5Ljg4IEwgMTE3LjUgMjMyLjg4IEwgMTIxIDIzNC42MyBMIDEyNC41IDIzMi44OCBaIiBmaWxsPSIjZDFkMWQxIiBzdHJva2U9IiNkMWQxZDEiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxyZWN0IHg9IjQxIiB5PSIxNjEiIHdpZHRoPSIxNjAiIGhlaWdodD0iNDAiIHJ4PSI2IiByeT0iNiIgZmlsbD0iI2ZmZmZmZiIgc3Ryb2tlPSIjYzRjNGM0IiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSAtMC41KSI+PHN3aXRjaD48Zm9yZWlnbk9iamVjdCBzdHlsZT0ib3ZlcmZsb3c6IHZpc2libGU7IHRleHQtYWxpZ246IGxlZnQ7IiBwb2ludGVyLWV2ZW50cz0ibm9uZSIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ij48ZGl2IHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sIiBzdHlsZT0iZGlzcGxheTogZmxleDsgYWxpZ24taXRlbXM6IHVuc2FmZSBjZW50ZXI7IGp1c3RpZnktY29udGVudDogdW5zYWZlIGNlbnRlcjsgd2lkdGg6IDE1OHB4OyBoZWlnaHQ6IDFweDsgcGFkZGluZy10b3A6IDE4MXB4OyBtYXJnaW4tbGVmdDogNDJweDsiPjxkaXYgc3R5bGU9ImJveC1zaXppbmc6IGJvcmRlci1ib3g7IGZvbnQtc2l6ZTogMDsgdGV4dC1hbGlnbjogY2VudGVyOyAiPjxkaXYgc3R5bGU9ImRpc3BsYXk6IGlubGluZS1ibG9jazsgZm9udC1zaXplOiAxMXB4OyBmb250LWZhbWlseTogVmVyZGFuYTsgY29sb3I6ICMwMDAwMDA7IGxpbmUtaGVpZ2h0OiAxLjI7IHBvaW50ZXItZXZlbnRzOiBhbGw7IHdoaXRlLXNwYWNlOiBub3JtYWw7IHdvcmQtd3JhcDogbm9ybWFsOyAiPkFwcGVsIGQnb2ZmcmU8L2Rpdj48L2Rpdj48L2Rpdj48L2ZvcmVpZ25PYmplY3Q+PHRleHQgeD0iMTIxIiB5PSIxODQiIGZpbGw9IiMwMDAwMDAiIGZvbnQtZmFtaWx5PSJWZXJkYW5hIiBmb250LXNpemU9IjExcHgiIHRleHQtYW5jaG9yPSJtaWRkbGUiPkFwcGVsIGQnb2ZmcmU8L3RleHQ+PC9zd2l0Y2g+PC9nPjxwYXRoIGQ9Ik0gMjAxIDI2MSBMIDIyMSAyNjEgTCAyMjEgMTA3LjM3IiBmaWxsPSJub25lIiBzdHJva2U9IiNkMWQxZDEiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9InN0cm9rZSIvPjxwYXRoIGQ9Ik0gMjIxIDEwMi4xMiBMIDIyNC41IDEwOS4xMiBMIDIyMSAxMDcuMzcgTCAyMTcuNSAxMDkuMTIgWiIgZmlsbD0iI2QxZDFkMSIgc3Ryb2tlPSIjZDFkMWQxIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJhbGwiLz48cmVjdCB4PSI0MSIgeT0iMjQxIiB3aWR0aD0iMTYwIiBoZWlnaHQ9IjQwIiByeD0iNiIgcnk9IjYiIGZpbGw9IiNmZmZmZmYiIHN0cm9rZT0iI2M0YzRjNCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiAxNThweDsgaGVpZ2h0OiAxcHg7IHBhZGRpbmctdG9wOiAyNjFweDsgbWFyZ2luLWxlZnQ6IDQycHg7Ij48ZGl2IHN0eWxlPSJib3gtc2l6aW5nOiBib3JkZXItYm94OyBmb250LXNpemU6IDA7IHRleHQtYWxpZ246IGNlbnRlcjsgIj48ZGl2IHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogMTFweDsgZm9udC1mYW1pbHk6IFZlcmRhbmE7IGNvbG9yOiAjMDAwMDAwOyBsaW5lLWhlaWdodDogMS4yOyBwb2ludGVyLWV2ZW50czogYWxsOyB3aGl0ZS1zcGFjZTogbm9ybWFsOyB3b3JkLXdyYXA6IG5vcm1hbDsgIj5EZXZpcyBmb3Vybmlzc2V1cjwvZGl2PjwvZGl2PjwvZGl2PjwvZm9yZWlnbk9iamVjdD48dGV4dCB4PSIxMjEiIHk9IjI2NCIgZmlsbD0iIzAwMDAwMCIgZm9udC1mYW1pbHk9IlZlcmRhbmEiIGZvbnQtc2l6ZT0iMTFweCIgdGV4dC1hbmNob3I9Im1pZGRsZSI+RGV2aXMgZm91cm5pc3NldXI8L3RleHQ+PC9zd2l0Y2g+PC9nPjxwYXRoIGQ9Ik0gMzIxIDEyMSBMIDMyMSAxNTQuNjMiIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2QxZDFkMSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSAzMjEgMTU5Ljg4IEwgMzE3LjUgMTUyLjg4IEwgMzIxIDE1NC42MyBMIDMyNC41IDE1Mi44OCBaIiBmaWxsPSIjZDFkMWQxIiBzdHJva2U9IiNkMWQxZDEiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxwYXRoIGQ9Ik0gNDAxIDEwMSBMIDQyMSAxMDEgTCA0MjEgMTQxIEwgNDM0LjYzIDE0MSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMDAwMDAwIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJzdHJva2UiLz48cGF0aCBkPSJNIDQzOS44OCAxNDEgTCA0MzIuODggMTQ0LjUgTCA0MzQuNjMgMTQxIEwgNDMyLjg4IDEzNy41IFoiIGZpbGw9IiMwMDAwMDAiIHN0cm9rZT0iIzAwMDAwMCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PHBhdGggZD0iTSA0MDEgMTAxIEwgNDIxIDEwMSBMIDQyMSA2MSBMIDQzNC42MyA2MSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMDAwMDAwIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJzdHJva2UiLz48cGF0aCBkPSJNIDQzOS44OCA2MSBMIDQzMi44OCA2NC41IEwgNDM0LjYzIDYxIEwgNDMyLjg4IDU3LjUgWiIgZmlsbD0iIzAwMDAwMCIgc3Ryb2tlPSIjMDAwMDAwIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHBvaW50ZXItZXZlbnRzPSJhbGwiLz48cmVjdCB4PSIyNDEiIHk9IjgxIiB3aWR0aD0iMTYwIiBoZWlnaHQ9IjQwIiByeD0iNiIgcnk9IjYiIGZpbGw9IiNlOGU4ZTgiIHN0cm9rZT0iI2M0YzRjNCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiAxNThweDsgaGVpZ2h0OiAxcHg7IHBhZGRpbmctdG9wOiAxMDFweDsgbWFyZ2luLWxlZnQ6IDI0MnB4OyI+PGRpdiBzdHlsZT0iYm94LXNpemluZzogYm9yZGVyLWJveDsgZm9udC1zaXplOiAwOyB0ZXh0LWFsaWduOiBjZW50ZXI7ICI+PGRpdiBzdHlsZT0iZGlzcGxheTogaW5saW5lLWJsb2NrOyBmb250LXNpemU6IDExcHg7IGZvbnQtZmFtaWx5OiBWZXJkYW5hOyBjb2xvcjogIzAwMDAwMDsgbGluZS1oZWlnaHQ6IDEuMjsgcG9pbnRlci1ldmVudHM6IGFsbDsgd2hpdGUtc3BhY2U6IG5vcm1hbDsgd29yZC13cmFwOiBub3JtYWw7ICI+PGIgc3R5bGU9ImZvbnQtc2l6ZTogMTFweCI+PGZvbnQgY29sb3I9IiMwMDAwZmYiIHN0eWxlPSJmb250LXNpemU6IDExcHgiPkNvbW1hbmRlIGZvdXJuaXNzZXVyPC9mb250PjwvYj48L2Rpdj48L2Rpdj48L2Rpdj48L2ZvcmVpZ25PYmplY3Q+PHRleHQgeD0iMzIxIiB5PSIxMDQiIGZpbGw9IiMwMDAwMDAiIGZvbnQtZmFtaWx5PSJWZXJkYW5hIiBmb250LXNpemU9IjExcHgiIHRleHQtYW5jaG9yPSJtaWRkbGUiPkNvbW1hbmRlIGZvdXJuaXNzZXVyPC90ZXh0Pjwvc3dpdGNoPjwvZz48cmVjdCB4PSIyNDEiIHk9IjE2MSIgd2lkdGg9IjE2MCIgaGVpZ2h0PSI0MCIgcng9IjYiIHJ5PSI2IiBmaWxsPSIjZmZmZmZmIiBzdHJva2U9IiNjNGM0YzQiIHBvaW50ZXItZXZlbnRzPSJhbGwiLz48ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMC41IC0wLjUpIj48c3dpdGNoPjxmb3JlaWduT2JqZWN0IHN0eWxlPSJvdmVyZmxvdzogdmlzaWJsZTsgdGV4dC1hbGlnbjogbGVmdDsiIHBvaW50ZXItZXZlbnRzPSJub25lIiB3aWR0aD0iMTAwJSIgaGVpZ2h0PSIxMDAlIiByZXF1aXJlZEZlYXR1cmVzPSJodHRwOi8vd3d3LnczLm9yZy9UUi9TVkcxMS9mZWF0dXJlI0V4dGVuc2liaWxpdHkiPjxkaXYgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWwiIHN0eWxlPSJkaXNwbGF5OiBmbGV4OyBhbGlnbi1pdGVtczogdW5zYWZlIGNlbnRlcjsganVzdGlmeS1jb250ZW50OiB1bnNhZmUgY2VudGVyOyB3aWR0aDogMTU4cHg7IGhlaWdodDogMXB4OyBwYWRkaW5nLXRvcDogMTgxcHg7IG1hcmdpbi1sZWZ0OiAyNDJweDsiPjxkaXYgc3R5bGU9ImJveC1zaXppbmc6IGJvcmRlci1ib3g7IGZvbnQtc2l6ZTogMDsgdGV4dC1hbGlnbjogY2VudGVyOyAiPjxkaXYgc3R5bGU9ImRpc3BsYXk6IGlubGluZS1ibG9jazsgZm9udC1zaXplOiAxMXB4OyBmb250LWZhbWlseTogVmVyZGFuYTsgY29sb3I6ICMwMDAwMDA7IGxpbmUtaGVpZ2h0OiAxLjI7IHBvaW50ZXItZXZlbnRzOiBhbGw7IHdoaXRlLXNwYWNlOiBub3JtYWw7IHdvcmQtd3JhcDogbm9ybWFsOyAiPsOJY3JpdHVyZSBkZSBwYWllbWVudDxiciBzdHlsZT0iZm9udC1zaXplOiAxMXB4IiAvPihBY29tcHRlKTwvZGl2PjwvZGl2PjwvZGl2PjwvZm9yZWlnbk9iamVjdD48dGV4dCB4PSIzMjEiIHk9IjE4NCIgZmlsbD0iIzAwMDAwMCIgZm9udC1mYW1pbHk9IlZlcmRhbmEiIGZvbnQtc2l6ZT0iMTFweCIgdGV4dC1hbmNob3I9Im1pZGRsZSI+w4ljcml0dXJlIGRlIHBhaWVtZW50Li4uPC90ZXh0Pjwvc3dpdGNoPjwvZz48cmVjdCB4PSI0NDEiIHk9IjQxIiB3aWR0aD0iMTYwIiBoZWlnaHQ9IjQwIiByeD0iNiIgcnk9IjYiIGZpbGw9IiNmZmZmZmYiIHN0cm9rZT0iI2M0YzRjNCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiAxNThweDsgaGVpZ2h0OiAxcHg7IHBhZGRpbmctdG9wOiA2MXB4OyBtYXJnaW4tbGVmdDogNDQycHg7Ij48ZGl2IHN0eWxlPSJib3gtc2l6aW5nOiBib3JkZXItYm94OyBmb250LXNpemU6IDA7IHRleHQtYWxpZ246IGNlbnRlcjsgIj48ZGl2IHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogMTFweDsgZm9udC1mYW1pbHk6IFZlcmRhbmE7IGNvbG9yOiAjMDAwMDAwOyBsaW5lLWhlaWdodDogMS4yOyBwb2ludGVyLWV2ZW50czogYWxsOyB3aGl0ZS1zcGFjZTogbm9ybWFsOyB3b3JkLXdyYXA6IG5vcm1hbDsgIj5SZcOndSBkJ2FjaGF0PC9kaXY+PC9kaXY+PC9kaXY+PC9mb3JlaWduT2JqZWN0Pjx0ZXh0IHg9IjUyMSIgeT0iNjQiIGZpbGw9IiMwMDAwMDAiIGZvbnQtZmFtaWx5PSJWZXJkYW5hIiBmb250LXNpemU9IjExcHgiIHRleHQtYW5jaG9yPSJtaWRkbGUiPlJlw6d1IGQnYWNoYXQ8L3RleHQ+PC9zd2l0Y2g+PC9nPjxwYXRoIGQ9Ik0gNjAxIDE0MSBMIDYzNC42MyAxNDEiIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2QxZDFkMSIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSA2MzkuODggMTQxIEwgNjMyLjg4IDE0NC41IEwgNjM0LjYzIDE0MSBMIDYzMi44OCAxMzcuNSBaIiBmaWxsPSIjZDFkMWQxIiBzdHJva2U9IiNkMWQxZDEiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxyZWN0IHg9IjQ0MSIgeT0iMTIxIiB3aWR0aD0iMTYwIiBoZWlnaHQ9IjQwIiByeD0iNiIgcnk9IjYiIGZpbGw9IiNmZmZmZmYiIHN0cm9rZT0iI2M0YzRjNCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiAxNThweDsgaGVpZ2h0OiAxcHg7IHBhZGRpbmctdG9wOiAxNDFweDsgbWFyZ2luLWxlZnQ6IDQ0MnB4OyI+PGRpdiBzdHlsZT0iYm94LXNpemluZzogYm9yZGVyLWJveDsgZm9udC1zaXplOiAwOyB0ZXh0LWFsaWduOiBjZW50ZXI7ICI+PGRpdiBzdHlsZT0iZGlzcGxheTogaW5saW5lLWJsb2NrOyBmb250LXNpemU6IDExcHg7IGZvbnQtZmFtaWx5OiBWZXJkYW5hOyBjb2xvcjogIzAwMDAwMDsgbGluZS1oZWlnaHQ6IDEuMjsgcG9pbnRlci1ldmVudHM6IGFsbDsgd2hpdGUtc3BhY2U6IG5vcm1hbDsgd29yZC13cmFwOiBub3JtYWw7ICI+RmFjdHVyZSBkJ2FjaGF0PC9kaXY+PC9kaXY+PC9kaXY+PC9mb3JlaWduT2JqZWN0Pjx0ZXh0IHg9IjUyMSIgeT0iMTQ0IiBmaWxsPSIjMDAwMDAwIiBmb250LWZhbWlseT0iVmVyZGFuYSIgZm9udC1zaXplPSIxMXB4IiB0ZXh0LWFuY2hvcj0ibWlkZGxlIj5GYWN0dXJlIGQnYWNoYXQ8L3RleHQ+PC9zd2l0Y2g+PC9nPjxyZWN0IHg9IjY0MSIgeT0iMTIxIiB3aWR0aD0iMTYwIiBoZWlnaHQ9IjQwIiByeD0iNiIgcnk9IjYiIGZpbGw9IiNmZmZmZmYiIHN0cm9rZT0iI2M0YzRjNCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiAxNThweDsgaGVpZ2h0OiAxcHg7IHBhZGRpbmctdG9wOiAxNDFweDsgbWFyZ2luLWxlZnQ6IDY0MnB4OyI+PGRpdiBzdHlsZT0iYm94LXNpemluZzogYm9yZGVyLWJveDsgZm9udC1zaXplOiAwOyB0ZXh0LWFsaWduOiBjZW50ZXI7ICI+PGRpdiBzdHlsZT0iZGlzcGxheTogaW5saW5lLWJsb2NrOyBmb250LXNpemU6IDExcHg7IGZvbnQtZmFtaWx5OiBWZXJkYW5hOyBjb2xvcjogIzAwMDAwMDsgbGluZS1oZWlnaHQ6IDEuMjsgcG9pbnRlci1ldmVudHM6IGFsbDsgd2hpdGUtc3BhY2U6IG5vcm1hbDsgd29yZC13cmFwOiBub3JtYWw7ICI+w4ljcml0dXJlIGRlIHBhaWVtZW50PC9kaXY+PC9kaXY+PC9kaXY+PC9mb3JlaWduT2JqZWN0Pjx0ZXh0IHg9IjcyMSIgeT0iMTQ0IiBmaWxsPSIjMDAwMDAwIiBmb250LWZhbWlseT0iVmVyZGFuYSIgZm9udC1zaXplPSIxMXB4IiB0ZXh0LWFuY2hvcj0ibWlkZGxlIj7DiWNyaXR1cmUgZGUgcGFpZW1lbnQ8L3RleHQ+PC9zd2l0Y2g+PC9nPjxyZWN0IHg9IjEiIHk9IjEiIHdpZHRoPSI4NDAiIGhlaWdodD0iMzIwIiByeD0iMTkuMiIgcnk9IjE5LjIiIGZpbGw9Im5vbmUiIHN0cm9rZT0iI2QxZDFkMSIgc3Ryb2tlLXdpZHRoPSIyIiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PC9nPjxzd2l0Y2g+PGcgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ii8+PGEgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtNSkiIHhsaW5rOmhyZWY9Imh0dHBzOi8vd3d3LmRpYWdyYW1zLm5ldC9kb2MvZmFxL3N2Zy1leHBvcnQtdGV4dC1wcm9ibGVtcyIgdGFyZ2V0PSJfYmxhbmsiPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIGZvbnQtc2l6ZT0iMTBweCIgeD0iNTAlIiB5PSIxMDAlIj5WaWV3ZXIgZG9lcyBub3Qgc3VwcG9ydCBmdWxsIFNWRyAxLjE8L3RleHQ+PC9hPjwvc3dpdGNoPjwvc3ZnPg==
```


---

Pour accéder à la **commande fournisseur**, allez sur :

> Accueil > Achats > **Commande fournisseur**

![commande_fournisseur.png](/buying/purchase-order/commande_fournisseur.png)

## 1. Prérequis avant utilisation

Avant de créer et d'utiliser un appel d'offre, il est conseillé de créer d'abord les éléments suivants:

1. **[Un fournisseur](/fr/buying/supplier)**
2. **[Un article](/fr/stocks/item)**

## 2. Comment créer une commande fournisseur

### 2.1 Informations de base
- Accédez à la liste des **commandes fournisseurs**, cliquez sur **:heavy_plus_sign:Ajouter Commande fournisseur**.
- Sélectionnez le **fournisseur** dans la liste.
- Indiquez la **date de création de la commande** et la **date demandé pour le**.

![nouvelle_commande_fournisseur.png](/buying/purchase-order/nouvelle_commande_fournisseur.png)

### 2.2 Adresse et contact

- L'**adresse** et le **contact** seront récupérés si vous les avez enregistrés dans la "**Fiche fournisseur**".

### 2.3 Devise et prix

- La **devise** renseigné par défaut est celle qui est indiquée dans les **"Paramètres d'achat"** ou dans les informations de la **"Fiche fournisseur"**.

### 2.4 Articles de la commande fournisseur

- Entrez le **code article**, sélectionnez la **quantité**. Le tarif sera récupéré si vous avez défini le tarif d'achat standard pour l'article dans **Prix de l'article .**

![articles_-_achat.png](/buying/purchase-order/articles_-_achat.png)

**Informations articles** à complèter si vous avez un message "champs Obligatoires Requis dans la table Articles, Ligne X > **Description**"

- Il suffit de cliquer sur "**Modifier**" dans ligne produit.

![articles_-_informations_supplémentaires.png](/buying/purchase-order/articles_-_informations_supplémentaires.png)

### 2.5 Taxes et frais

Si votre fournisseur vous facture des taxes supplémentaires ou d'autres frais comme des frais d'expédition ou d'assurance, vous pouvez l'ajouter dans cette section. Cela vous aidera à suivre avec précision vos coûts.

De plus, si certains de ces frais ajoutent à la valeur du produit, vous devrez les mentionner dans le tableau des taxes. Vous pouvez également utiliser des modèles pour vos impôts. Pour plus d'informations sur la configuration de vos taxes, consultez le modèle de taxes et frais d'achat.

#### **Règle de livraison** 

Une règle d'expédition permet de définir le coût d'expédition d'un article. Le coût augmentera généralement avec la distance d'expédition. Pour en savoir plus, visitez la page des règles d'expédition.

Ceci est très facile à suivre dans DOKOS puisque chaque taxe fiscale est également un compte. Idéalement, vous devez créer deux comptes pour chaque type de TVA que vous payez et collectez, «Achat TVA-X» (actif) et «Vente TVA-X» (responsabilité), ou quelque chose à cet effet.

### 2.6 Remise supplémentaire

Outre l'enregistrement de la remise par article, vous pouvez ajouter une remise à l'ensemble du bon de commande dans cette section. Cette remise pourrait être basée sur le total général, c'est-à-dire après impôts / charges ou total net, c'est-à-dire avant taxes / charges. La remise supplémentaire peut être appliquée sous forme de pourcentage ou de montant.

### 2.7 Termes et conditions

Dans les transactions de vente / achat, il peut y avoir certaines conditions générales sur la base desquelles le fournisseur fournit des biens ou des services au client. Vous pouvez appliquer les conditions générales aux transactions aux transactions et elles apparaîtront lors de l'impression du document.

### 2.8 Paramètres d'impression

- **En-tête de lettre**
Vous pouvez imprimer votre demande de devis / bon de commande sur le papier à en-tête de votre entreprise. En savoir plus ici .

«Regrouper les mêmes éléments» regroupera les mêmes éléments ajoutés plusieurs fois dans le tableau des éléments. Cela peut être vu lorsque votre impression.

- **Imprimer les titres** 
Les titres de vos documents peuvent être modifiés.

### 2.9 Informations additionnelles

Cette section affiche le statut de la commande fournisseur. S'il s'agit d'une commande interentreprises.

### A noter

Les détails que vous remplissez ici, tels que la règle d'expédition, les taxes, la remise, les conditions générales, etc., proviennent de votre fournisseur et peuvent être enregistrés pour un suivi précis.

**A savoir** :

**La catégorie de taxe** sera extraite de la fiche fournisseur si elle est définie
**Les paramètres d'impression** permettent de modifier l'impression de l'offre fournisseur
**Les conditions générales** ici sont celles de votre fournisseur

## 3. Récupération d'articles

Une commande d'achat peut être créée automatiquement à partir de deux méthodes.

### 3.1 Depuis la demande de matériel

Pour accéder, allez sur :
> Obtenir les articles de > **Demande de matériel**

Pour utiliser cette méthode il faut au préalable avoir **[Créer une demande de matériel](/fr/stocks/material-request)**

![demande_de_matériel_-_commande_fournisseur.png](/buying/purchase-order/demande_de_matériel_-_commande_fournisseur.png)

Les articles peuvent être récupérés automatiquement dans la commande d'achat à partir des demandes de matériel ouvertes . Pour que cela fonctionne, les étapes suivantes doivent être effectuées:

- Sélectionnez un fournisseur dans le bon de commande.
- Définissez le fournisseur par défaut dans le formulaire Article sous Valeurs par défaut de l'article .
- Une demande de matériel doit présenter un type «achat».
- Cliquez sur le bouton Obtenir les articles des demandes de matériel ouvertes sous le nom du fournisseur. 

Une boîte de dialogue apparaîtra avec les demandes de matériel contenant des articles pour lesquels le fournisseur par défaut est le même que celui sélectionné dans le bon de commande. En sélectionnant les demandes de matériel et en cliquant sur Obtenir les articles , les articles seront extraits des demandes de matériel.

### 3.2 Depuis le devis fournisseur

Pour accéder, allez sur :
> Obtenir les articles de > **Devis fournisseur**

Pour utiliser cette méthode il faut au préalable avoir **[Créer un devis fournisseur](/fr/buying/order-supplier)**

![devis_fournisseur_-_commande_fournisseur.png](/buying/purchase-order/devis_fournisseur_-_commande_fournisseur.png)

### 4. Après l'enregistrement

Une fois que vous avez «Enregistré» votre commande fournisseur, vous pouvez effectuer des actions telles que :

-	Ajouter des articles
- Mettre à jour des articles
- Supprimer des articles 

> Vous ne pouvez pas supprimer les éléments qui ont déjà été reçus.
{.is-warning}

- **Statut** : une fois soumis, vous pouvez conserver la commande fournisseur ou la fermer.

- **Créer** : à partir d'une commande fournisseur soumise, vous pouvez créer les éléments suivants:

	- **Reçu d'achat** : Un reçu indiquant que vous avez reçu les articles.
	- **Facture d'achat** : Une facture / facture pour la commande fournisseur.
	- **Saisie de paiement** : Une entrée de paiement indique que le paiement 	a été effectué par rapport à un bon de commande.
	- 	**Écriture de journal** : Une entrée de journal est enregistrée dans le 	grand livre.



