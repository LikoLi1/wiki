---
title: Production
description: 
published: true
date: 2021-06-12T07:41:28.241Z
tags: 
editor: markdown
dateCreated: 2021-05-28T13:03:44.906Z
---

# Production

Le module Production de DOKOS regroupe toutes les fonctionnalités nécessaires dont une entreprise de production aurait besoin. Il existe des options pour prendre un ordre de travail, saisir le stock d'articles, créer un plan de production, créer une entrée de temps d'arrêt, gérer la nomenclature pour les produits finis / éléments de modèle, générer des rapports, etc.

Différents types de prroduction comme la production sur stock, la production sur commande et l'ingénierie sur commande peuvent être gérés dans le module de production de DOKOS.

--- 

Pour accéder au module de **Production**, allez sur :

> Accueil > **Production**

## 1. Fonctionnalités de base

- [1. Introduction à la production](/fr/manufacturing/onboarding)
- [2. Ordre de travail](/fr/manufacturing/work-order)
- [3. Plan de production](/fr/manufacturing/production-plan)
- [4. Écriture de stock](/fr/stocks/stock-entry)
- [5. Carte de travail](/fr/manufacturing/job-card)
- [6. Écriture de temps de travail](/fr/manufacturing/downtime-entry)
{.links-list}

## 2. Liste de matériaux

- [1. Article](/fr/stocks/item)
- [2. Nomenclature](/fr/manufacturing/bom)
- [3. Opération](/fr/manufacturing/operation)
- [4. Routage](/fr/manufacturing/routing)
- [5. Station de travail](/fr/manufacturing/workstation)
{.links-list}

## 3. Paramètres de production

- [Paramètres de production](/fr/manufacturing/manufacturing-settings)
- [Tableau de bord de production](/fr/manufacturing/manufacturing)
- [Outil de mise à jour de la nomenclature](/fr/manufacturing/bom-update-tool)

{.links-list}

