---
title: Intégrations
description: 
published: true
date: 2021-05-25T08:31:49.426Z
tags: 
editor: markdown
dateCreated: 2020-11-26T16:38:48.410Z
---

# Paiements
- [GoCardless](/fr/integrations/gocardless)
- [Stripe](/fr/integrations/stripe)
- [Paypal](/fr/integrations/paypal)
{.links-list}

# Agendas / Drive / Emails / Contacts
- [Google](/fr/integrations/google)
{.links-list}

# Connecteurs
- [Zapier](/fr/integrations/zapier)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
