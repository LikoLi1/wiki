---
title: Comptabilité
description: 
published: true
date: 2021-05-25T08:31:24.441Z
tags: 
editor: markdown
dateCreated: 2020-11-26T12:24:00.536Z
---

# Fonctionnalités

- [Journal Comptable](/fr/accounting/accounting-journal)
- [Facture](/fr/accounting/sales-invoice)
- [Facture d'acompte](/fr/accounting/down-payment-invoice)
- [Ecriture de paiement](/fr/accounting/payment-entry)
- [Demande de paiement](/fr/accounting/payment-request)
- [Prélèvement Sepa](/fr/accounting/sepa-direct-debit)
- [Passerelles de paiement](/fr/accounting/payment-gateways)
- [Rapprochement bancaire](/fr/accounting/bank-reconciliation)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
