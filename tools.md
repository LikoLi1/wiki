---
title: Utilisation de l'ERP Dokos
description: 
published: true
date: 2021-05-28T10:35:58.505Z
tags: 
editor: markdown
dateCreated: 2021-05-27T12:51:59.094Z
---

# Utilisation de l'ERP Dokos
Apprenez à naviguer dans l'ERP de DOKOS. Découvrez l'interface utilisateur et les fonctionnalités utiles.

## Les outils pour démarrer

- [Liste à faire | To do](/fr/tools/to-do)
- [Notes](/fr/tools/notes)
- [Tableau de bord](/fr/tools/dashboard)
{.links-list}


## Les outils pour collaborer

- [Filtre de recherche](/fr/tools/search-filter)
{.links-list}


