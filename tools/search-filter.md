---
title: Filtre de recherche
description: 
published: true
date: 2021-05-27T14:13:15.668Z
tags: 
editor: markdown
dateCreated: 2021-05-27T14:12:02.903Z
---

# Filtre de recherche

Filtre de recherche est une option qui vous permet de filtrer les enregistrements en fonction d'une valeur particulière d'un champ spécifique dans un document.

Les filtres de recherche sont disponibles dans la vue Liste et dans le Générateur de rapports d'un type de document.

Chaque option de filtre comporte trois champs.

**1. Domaine** 

Sélectionnez le champ du document en fonction duquel vous souhaitez filtrer les enregistrements. Tous les champs d'un formulaire pourront être sélectionnés dans cette liste.

**2. Basé sur**

Pour le champ, il vous sera demandé de mettre une valeur du champ. Dans le champ «basé sur», vous pouvez définir des critères en fonction desquels le système recherchera le document.

**3. Valeur**

Ici, vous devrez entrer la valeur pour laquelle vous exécutez la recherche de document.

Ainsi, dans l'ensemble, un algorithme ou une équation de recherche des documents sera créé qui vous permettra de récupérer votre ser de documents souhaité.
Vous pouvez également appliquer plusieurs filtres à la fois. Pour supprimer un filtre spécifique, cliquez simplement sur le signe «x» dessus.

## Filtres par défaut 
Il existe des filtres prêts à l'emploi pour les vues qui peuvent être utilisés pour filtrer les résultats de la recherche. Les filtres par défaut pour tout type de document peuvent être définis à partir de l' option Personnaliser le formulaire

