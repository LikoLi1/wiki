---
title: Termes et paiement
description: 
published: true
date: 2021-05-07T07:38:00.503Z
tags: 
editor: markdown
dateCreated: 2021-05-07T07:34:31.136Z
---

# Termes et paiement
> Contenu bientôt disponible
{.is-warning}

#### Une condition de paiement permet de définir un calendrier selon lequel les paiements seront effectués.

Une condition de paiement définit une tranche de paiement spécifique. Par exemple, 50% de paiement à l'expédition et 50% à la livraison de l'article. Vous pouvez enregistrer les conditions de paiement de votre entreprise sur DOKOS et les inclure dans tous les documents du cycle de vente / achat. DOKOS effectuera toutes les écritures du grand livre en conséquence.

Dans DOKOS, le formulaire Conditions de paiement ne définit que les pourcentages de portion. Le calendrier de paiement réel peut facilement être appliqué à l'aide du modèle de conditions de paiement.

Vous pouvez utiliser les conditions de paiement dans les documents suivants:

- Facture de vente
- Facture d'achat
- Commande client
- Commande fournisseur
- Devis fournisseur
- Devis client

Pour accéder aux **termes et paiement**, accédez à:

> Accueil> Comptabilité> Masters comptables> **Termes et paiement**
