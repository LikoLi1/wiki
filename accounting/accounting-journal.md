---
title: Accounting Journal
description: 
published: true
date: 2020-11-27T07:13:42.744Z
tags: 
editor: undefined
dateCreated: 2020-11-26T12:20:03.069Z
---

# Accounting Journal

> :point_right: v2
{.is-info}


An accounting journal is an auxiliary ledger.
Dokos allows creating as many journals as necessary for your accounting.

### 1. How to create an accounting journal

`Accounting > Accounting Journal`

1. Select a name and a code for your accounting journal
2. If your journal is a bank or cash account, select the associated account.
3. Add a many condition lines as necessary (see below)

> Generaly it is recommended to have at least 4 default journals:
> - Sales
> - Purchases
> - Bank/Cash
> - General journal
> 
> You need to create one journal for each of your bank accounts or cash registers.
{.is-info}


### 2. Conditions

Each journal can be associated with one or several transactions.

> The journal "SALES" will be associated to document type "Sales Invoice".
> Each time a sales invoice will be validated, the corresponding accounting entries will be registered in the sales journal.
{.is-info}


It is also possible to add a condition for a specific transaction.

> The journal "CREDIT NOTES" will also be associated to document type "Sales Invoice", with condition `doc.is_return`
> 
> Everytime an invoice will be validated, it will be registered in the "SALES" journal and if it is a return/credit note it will be registered in the credit notes journal.
{.is-info}

Conditions must have the form `doc.{name of the field to validate}` and must return a True/False value.

Examples returning the same result:
```
doc.is_return
doc.is_return == 1
doc.is_return == True
```