---
title: Actifs
description: Immobilisations corporellles et incorporelles
published: true
date: 2021-05-25T08:31:09.497Z
tags: 
editor: markdown
dateCreated: 2020-12-21T07:47:33.124Z
---

# Démarrer

- [Enregistrer un nouvel actif immobilisé](/fr/assets/getting-started)
{.links-list}

Découvrez la communauté de DOKOS **<a href="https://community.dokos.io" target="_blank">ICI</a>** pour retrouver ou demander une information technique à un utilisateur ou un administrateur.
